package br.com.cefet.lp3.trabalho.ui;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.ui.controlador.ListaClientesUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.ConsultasUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.CrudFuncionariosUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.ListaFuncionariosUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.MenuUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.ListaProdutosUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.ListaVendasUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.RelatorioClientesUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.RelatorioProdutoVendaUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.RelatorioProdutosUIController;
import br.com.cefet.lp3.trabalho.ui.controlador.RelatorioVendasUIController;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Programa extends Application {

    private static Stage stage;

    static String telaAnterior = "";
    static Object obj;

    private static Scene loginScene;
    private static Scene menuScene;
    private static Scene consultaScene;
    private static Scene listaClienteScene;
    private static Scene listaVendaScene;
    private static Scene listaFuncionarioScene;
    private static Scene listaProdutoScene;
    private static Scene crudFuncionarioScene;
    private static Scene relatorioProdutoScene;
    private static Scene relatorioVendaScene;
    private static Scene relatorioClienteScene;
    private static Scene relatorioProdutoVendaScene;

    private static MenuUIController menu;
    private static ConsultasUIController consulta;
    private static ListaVendasUIController listaVenda;
    private static ListaClientesUIController listaCliente;
    private static ListaProdutosUIController listaProduto;
    private static ListaFuncionariosUIController listaFuncionario;
    private static CrudFuncionariosUIController crudFuncionario;
    private static RelatorioProdutosUIController relatorioProduto;
    private static RelatorioVendasUIController relatorioVenda;
    private static RelatorioClientesUIController relatorioCliente;
    private static RelatorioProdutoVendaUIController relatorioProdutoVenda;

    public static void main(String[] args) {
        Supermercado s = Supermercado.getInstance();
        System.out.println(s.getNome());

        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        stage = primaryStage;
        primaryStage.setTitle("Login");
        primaryStage.getIcons().add(new Image(getClass().getResource("controlador/imagens/MercaDjin.png").toExternalForm()));

        FXMLLoader loaderMenu = new FXMLLoader(getClass().getResource("fxml/MenuUI.fxml"));
        FXMLLoader loaderLogin = new FXMLLoader(getClass().getResource("fxml/LoginUI.fxml"));
        FXMLLoader loaderConsulta = new FXMLLoader(getClass().getResource("fxml/ConsultasUI.fxml"));
        FXMLLoader loaderListaVenda = new FXMLLoader(getClass().getResource("fxml/ListaVendasUI.fxml"));
        FXMLLoader loaderListaCliente = new FXMLLoader(getClass().getResource("fxml/ListaClientesUI.fxml"));
        FXMLLoader loaderListaProduto = new FXMLLoader(getClass().getResource("fxml/ListaProdutosUI.fxml"));
        FXMLLoader loaderListaFuncionario = new FXMLLoader(getClass().getResource("fxml/ListaFuncionariosUI.fxml"));
        FXMLLoader loaderCrudFuncionario = new FXMLLoader(getClass().getResource("fxml/CrudFuncionariosUI.fxml"));
        FXMLLoader loaderRelatorioProduto = new FXMLLoader(getClass().getResource("fxml/RelatorioProdutosUI.fxml"));
        FXMLLoader loaderRelatorioVenda = new FXMLLoader(getClass().getResource("fxml/RelatorioVendasUI.fxml"));
        FXMLLoader loaderRelatorioCliente = new FXMLLoader(getClass().getResource("fxml/RelatorioClientesUI.fxml"));
        FXMLLoader loaderRelatorioProdutoVenda = new FXMLLoader(getClass().getResource("fxml/RelatorioProdutoVendaUI.fxml"));

        Parent fxmlMenu = loaderMenu.load();
        Parent fxmlLogin = loaderLogin.load();
        Parent fxmlConsulta = loaderConsulta.load();
        Parent fxmlListaVenda = loaderListaVenda.load();
        Parent fxmlListaCliente = loaderListaCliente.load();
        Parent fxmlListaProduto = loaderListaProduto.load();
        Parent fxmlListaFuncionario = loaderListaFuncionario.load();
        Parent fxmlCrudFuncionario = loaderCrudFuncionario.load();
        Parent fxmlRelatorioProduto = loaderRelatorioProduto.load();
        Parent fxmlRelatorioVenda = loaderRelatorioVenda.load();
        Parent fxmlRelatorioCliente = loaderRelatorioCliente.load();
        Parent fxmlRelatorioProdutoVenda = loaderRelatorioProdutoVenda.load();

        loginScene = new Scene(fxmlLogin);
        menuScene = new Scene(fxmlMenu);
        consultaScene = new Scene(fxmlConsulta);
        listaClienteScene = new Scene(fxmlListaCliente);
        listaVendaScene = new Scene(fxmlListaVenda);
        listaFuncionarioScene = new Scene(fxmlListaFuncionario);
        listaProdutoScene = new Scene(fxmlListaProduto);
        crudFuncionarioScene = new Scene(fxmlCrudFuncionario);
        relatorioProdutoScene = new Scene(fxmlRelatorioProduto);
        relatorioVendaScene = new Scene(fxmlRelatorioVenda);
        relatorioClienteScene = new Scene(fxmlRelatorioCliente);
        relatorioProdutoVendaScene = new Scene(fxmlRelatorioProdutoVenda);

        menu = loaderMenu.getController();
        consulta = loaderConsulta.getController();
        listaCliente = loaderListaCliente.getController();
        listaVenda = loaderListaVenda.getController();
        listaFuncionario = loaderListaFuncionario.getController();
        listaProduto = loaderListaProduto.getController();
        crudFuncionario = loaderCrudFuncionario.getController();
        relatorioProduto = loaderRelatorioProduto.getController();
        relatorioVenda = loaderRelatorioVenda.getController();
        relatorioCliente = loaderRelatorioCliente.getController();
        relatorioProdutoVenda = loaderRelatorioProdutoVenda.getController();

        primaryStage.setScene(loginScene);
        //primaryStage.centerOnScreen();
        primaryStage.setResizable(true);
        primaryStage.setMinHeight(630);
        primaryStage.setMinWidth(530);
        primaryStage.show();
    }

    public static void changeScene(String fxml, String origem, Object objeto) {
        telaAnterior = origem;
        if (objeto != null) {
            if ("login".equals(origem)) {
                menu.atualizarHeader();
                menu.atualizarMsg();
            } else {
                obj = objeto;
                consulta.atualizarCampos();

            }

        }
        switch (fxml) {
            case "menu":
                stage.setScene(menuScene);
                if (origem.equals("login")) {
                    stage.centerOnScreen();
                }
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Menu Principal");
                break;
            case "consultas":
                stage.setScene(consultaScene);
                //  stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Consultas");
                break;
            case "clientes":
                stage.setScene(listaClienteScene);
                //    stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Clientes");
                break;
            case "vendas":
                stage.setScene(listaVendaScene);
                // stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Vendas");
                break;
            case "produtos":
                stage.setScene(listaProdutoScene);
                //stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Produtos");
                break;
            case "funcionarios":
                stage.setScene(listaFuncionarioScene);
                // stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Funcionarios");
                break;
            case "rprodutos":
                stage.setScene(relatorioProdutoScene);
                // stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Relatório de Produtos");
                break;
            case "rvendas":
                stage.setScene(relatorioVendaScene);
                // stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Relatório de Vendas");
                break;
            case "rclientes":
                stage.setScene(relatorioClienteScene);
                // stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Relatório de Endereços por Cliente");
                break;
            case "rprodutovendas":
                stage.setScene(relatorioProdutoVendaScene);
                // stage.centerOnScreen();
                stage.setMinHeight(650);
                stage.setMinWidth(1100);
                stage.setTitle("Relatório de Produtos por Venda");
                break;

        }
    }

    public static Object getObject() {
        return obj;
    }

    public static String getTelaAnterior() {
        return telaAnterior;
    }
}

//atual
// linear-gradient(to bottom right, #E04343, #B02D2C)
//antigo
//linear-gradient(to bottom right, #FFAF2A, #00CED1)

