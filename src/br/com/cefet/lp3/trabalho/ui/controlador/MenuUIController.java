/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Venda;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class MenuUIController implements Initializable {

    @FXML
    private Label lbWelcome;
    @FXML
    private Label lbVendasR;
    @FXML
    private Label lbVendasE;
    @FXML
    private Label lbVendasH;

    Supermercado s = Supermercado.getInstance();

    @FXML
    TableView tbVenda;
    @FXML
    TableColumn<Venda, Date> dataCol;
    @FXML
    TableColumn<Venda, String> pagamentoCol;
    @FXML
    TableColumn<Venda, Integer> parcelasCol;
    @FXML
    TableColumn<Venda, String> statusCol;
    @FXML
    TableColumn<Venda, Double> freteCol;
    @FXML
    TableColumn<Venda, Double> compraCol;
    @FXML
    TableColumn<Venda, Double> totalCol;
    @FXML
    TableColumn<Venda, String> clienteCol;
    @FXML
    TableColumn<Venda, String> funcionarioCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dataCol.setCellValueFactory(new PropertyValueFactory<>("dtVenda"));
        pagamentoCol.setCellValueFactory(new PropertyValueFactory<>("formaPagamento"));
        parcelasCol.setCellValueFactory(new PropertyValueFactory<>("qtdParcelas"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("statusEntrega"));
        freteCol.setCellValueFactory(new PropertyValueFactory<>("precoFrete"));
        compraCol.setCellValueFactory(new PropertyValueFactory<>("precoCompra"));
        totalCol.setCellValueFactory(new PropertyValueFactory<>("precoTotal"));
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("nmCliente"));
        funcionarioCol.setCellValueFactory(new PropertyValueFactory<>("nmFuncionario"));

        try {
            tbVenda.setItems(listaVenda());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }

    }

    private ObservableList<Venda> listaVenda() throws DaoException {
        ObservableList<Venda> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosVenda().size(); i++) {
            list.add(s.consultarTodosVenda().get(i));
        }
        return list;
    }

    private void preencherTabela() {
        try {
            tbVenda.getItems().forEach((_item) -> {
                tbVenda.getItems().clear();
            });
            List<Venda> vList = s.consultarTodosVenda();
            vList.forEach((v) -> {
                tbVenda.getItems().add(v);
            });
            tbVenda.autosize();
        } catch (DaoException e) {
            e.printStackTrace();
        }

    }

    @FXML
    private void clientes() {
        lbWelcome.requestFocus();
        Programa.changeScene("clientes", "menu", null);
    }

    @FXML
    private void vendas() {
        lbWelcome.requestFocus();
        Programa.changeScene("vendas", "menu", null);
    }

    @FXML
    private void produtos() {
        lbWelcome.requestFocus();
        Programa.changeScene("produtos", "menu", null);
    }

    @FXML
    private void funcionarios() {
        lbWelcome.requestFocus();
        Programa.changeScene("funcionarios", "menu", null);
    }

    @FXML
    private void sair() {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Fechar sistema");
        alert.setHeaderText("Você tem certeza que deseja sair?");
        alert.setContentText("Se tiver certeza, clique em OK para fechar o sistema");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            System.exit(0);

        } else {
            // ... user chose CANCEL or closed the dialog
        }
    }

    public void atualizarHeader() {
        Long longData = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String stringData = sdf.format(longData);

        try {
            int vendasH = 0;
            int vendasE = 0;
            lbVendasR.setText(Integer.toString(s.consultarTodosVenda().size()));
            for (int i = 0; i < s.consultarTodosVenda().size(); i++) {
                if (s.consultarTodosVenda().get(i).getDtVenda().toString().equals(stringData)) {
                    vendasH++;
                }
                if (!s.consultarTodosVenda().get(i).getStatusEntrega().equals("Entregue")) {
                    vendasE++;
                }
            }
            lbVendasH.setText(Integer.toString(vendasH));
            lbVendasE.setText(Integer.toString(vendasE));

        } catch (DaoException e) {
            System.out.println("Dao Exception" + e);
        }

    }

    public void atualizarMsg() {
        lbWelcome.setText("Bem vindo, " + s.getFuncionarioLogado().getNmFuncionario());
    }
}
