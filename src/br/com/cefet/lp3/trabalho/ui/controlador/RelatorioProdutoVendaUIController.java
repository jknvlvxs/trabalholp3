/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.ProdutoVenda;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RelatorioProdutoVendaUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();

    @FXML
    JFXTextField tfQtdMin;
    @FXML
    JFXTextField tfQtdMax;
    @FXML
    JFXTextField tfProduto;
    @FXML
    JFXButton btPesquisar;
    @FXML
    JFXButton btGerar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void clientes() {
        Programa.changeScene("clientes", "vendas", null);
    }

    @FXML
    private void produtos() {
        Programa.changeScene("produtos", "vendas", null);
    }

    @FXML
    private void funcionarios() {
        Programa.changeScene("funcionarios", "vendas", null);
    }

    @FXML
    private void voltar() {
        Programa.changeScene("menu", "vendas", null);
    }

//    private List<Venda> getList() throws DaoException {;
//        List<Venda> all = s.consultarTodosVenda();
//        List<Venda> vList = new ArrayList();
//
//        //variaveis 
//        Double vMin;
//        Double vMax;
//        String status;
//
//        try {
//            vMin = Double.parseDouble(tfValMin.getText());
//        } catch (NumberFormatException e) {
//            vMin = 0.0;
//        }
//
//        try {
//            vMax = Double.parseDouble(tfValMax.getText());
//        } catch (NumberFormatException e) {
//            vMax = 99999999.0;
//        }
//
//        for (int i = 0; i < all.size(); i++) {
//            if (all.get(i).getPrecoTotal() >= vMin && all.get(i).getPrecoTotal() <= vMax) {
//                if (tfCidade.getText().isEmpty() || all.get(i).getStatusEntrega().equals(tfCidade.getText())) {
//                    vList.add(all.get(i));
//                }
//            }
//        }
//        return vList;
//    }
    @FXML
    private void gerar() throws DaoException {
        int qtdMin;
        int qtdMax;

        try {
            qtdMin = Integer.parseInt(tfQtdMin.getText());
        } catch (NumberFormatException e) {
            qtdMin = 0;
        }

        try {
            qtdMax = Integer.parseInt(tfQtdMax.getText());
        } catch (NumberFormatException e) {
            qtdMax = Integer.MAX_VALUE;
        }
        emitirRelatorio(s.relatorioProdutoVenda(qtdMin, qtdMax, tfProduto.getText()));
    }

    public void emitirRelatorio(List<ProdutoVenda> vList) {
        String reportSource = "./ProdutoVenda.jasper";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("nomeEmpresa", "MercaDjin");

        try {
            JRBeanCollectionDataSource jrBean = new JRBeanCollectionDataSource(vList);
            JasperPrint jasperPrint = JasperFillManager.fillReport(reportSource, params, jrBean);

            JasperViewer.viewReport(jasperPrint, false);

        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }
}
