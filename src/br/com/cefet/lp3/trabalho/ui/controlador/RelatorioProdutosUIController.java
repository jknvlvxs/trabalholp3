/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RelatorioProdutosUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();

    @FXML
    JFXTextField tfValMin;
    @FXML
    JFXTextField tfValMax;
    @FXML
    JFXTextField tfCategoria;
    @FXML
    JFXButton btPesquisar;
    @FXML
    JFXButton btGerar;
    @FXML
    TableView tbProduto;
    @FXML
    TableColumn<Produto, Integer> codigoCol;
    @FXML
    TableColumn<Produto, String> nomeCol;
    @FXML
    TableColumn<Produto, Double> precoCol;
    @FXML
    TableColumn<Produto, String> categoriaCol;
    @FXML
    TableColumn<Produto, Integer> quantidadeCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdProduto"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmProduto"));
        precoCol.setCellValueFactory(new PropertyValueFactory<>("precoProduto"));
        categoriaCol.setCellValueFactory(new PropertyValueFactory<>("categoriaProduto"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantEstoque"));

        atualizarTabela();
    }

    private void atualizarTabela(List<Produto> produtos) {
        for (int i = 0; i < tbProduto.getItems().size(); i++) {
            tbProduto.getItems().clear();
        }
        for (Produto p : produtos) {
            tbProduto.getItems().add(p);
        }
    }

    public void atualizarTabela() {
        try {
            tbProduto.setItems(listaProduto());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Produto> listaProduto() throws DaoException {
        ObservableList<Produto> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosProduto().size(); i++) {
            list.add(s.consultarTodosProduto().get(i));
        }
        return list;
    }

    //MENU
    @FXML
    private void clientes() {
        Programa.changeScene("clientes", "produtos", null);
    }

    @FXML
    private void vendas() {
        Programa.changeScene("vendas", "produtos", null);
    }

    @FXML
    private void funcionarios() {
        Programa.changeScene("funcionarios", "produtos", null);

    }

    @FXML
    private void voltar() {
        Programa.changeScene("menu", "produtos", null);
    }

    @FXML
    private void pesquisar() throws DaoException {
        // List<Produto> pList = getList();
        //atualizarTabela(pList);
        Double vMin = null;
        Double vMax = null;
        String categoria;

        try {
            vMin = Double.parseDouble(tfValMin.getText());
        } catch (NumberFormatException e) {
            vMin = 0.0;
        }

        try {
            vMax = Double.parseDouble(tfValMax.getText());
        } catch (NumberFormatException e) {
            vMax = Double.POSITIVE_INFINITY;
        }
        atualizarTabela(s.relatorioProduto(vMin, vMax, tfCategoria.getText()));
    }

    @FXML
    private void gerar() throws DaoException {
        Double vMin;
        Double vMax;
        String categoria;

        try {
            vMin = Double.parseDouble(tfValMin.getText());
        } catch (NumberFormatException e) {
            vMin = 0.0;
        }

        try {
            vMax = Double.parseDouble(tfValMax.getText());
        } catch (NumberFormatException e) {
            vMax = Double.POSITIVE_INFINITY;
        }
        emitirRelatorio(s.relatorioProduto(vMin, vMax, tfCategoria.getText()));
    }

    public void emitirRelatorio(List<Produto> cList) {
        String reportSource = "./RelatorioProdutos.jasper";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("nomeEmpresa", "MercaDjin");

        try {
            JRBeanCollectionDataSource jrBean = new JRBeanCollectionDataSource(cList);
            JasperPrint jasperPrint = JasperFillManager.fillReport(reportSource, params, jrBean);

            JasperViewer.viewReport(jasperPrint, false);

        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }
}
