/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.entidade.Endereco;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.util.Validacao;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author ADMIN
 */
public class CrudEnderecosUIController implements Initializable {

    Stage tela;
    Supermercado s = Supermercado.getInstance();
    private String origem;
    private Endereco endereco;
    @FXML
    private JFXTextField tfCodigo;
    @FXML
    private JFXTextField tfLogradouro;
    @FXML
    private JFXTextField tfNumero;
    @FXML
    private JFXTextField tfMunicipio;
    @FXML
    private JFXTextField tfBairro;
    @FXML
    private JFXTextField tfCep;
    @FXML
    private JFXTextField tfCodigoCliente;
    @FXML
    private JFXButton btCancelar;
    @FXML
    private JFXButton btConfirmar;
    @FXML
    private ComboBox<String> cbUf;
    @FXML
    private Label lbTitulo;

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btConfirmar.setDefaultButton(true);
    }

    void ajustarTela(String origem, Endereco e, Cliente c) throws DaoException {
        setOrigem(origem);
        switch (origem) {
            case "cadastrar":
                tfCodigo.setText(Integer.toString(s.getProximoCodigoEndereco()));

                tfCodigoCliente.setText(Integer.toString(c.getCdCliente()));
                break;
            case "editar":
                setEndereco(e);
                tfCodigoCliente.setText(Integer.toString(c.getCdCliente()));
                break;
            case "visualizar":
                lbTitulo.setText("VISUALIZAÇÃO DE ENDEREÇO");
                tfLogradouro.setDisable(true);
                tfNumero.setDisable(true);
                tfMunicipio.setDisable(true);
                tfBairro.setDisable(true);
                tfCodigoCliente.setDisable(true);
                tfCep.setDisable(true);
                cbUf.setDisable(true);
                tfCodigo.setText(Integer.toString(e.getCdEndereco()));
                tfLogradouro.setText(e.getLogradouroEnd());
                tfNumero.setText(e.getNumeroEnd());
                tfMunicipio.setText(e.getMunicipioEnd());
                tfBairro.setText(e.getBairroEnd());
                tfCep.setText(e.getCepEnd());
                btCancelar.setVisible(false);
                btConfirmar.setText("Fechar");
                tfCodigoCliente.setText(Integer.toString(c.getCdCliente()));
                cbUf.setValue(e.getUfEnd());
                break;
        }
    }

    @FXML
    private void cancelar() {
        this.getTela().close();
    }

    @FXML
    private void confirmar() throws ControleException, DaoException {
        Endereco e;
        switch (origem) {
            case "cadastrar":
                if (validar()) {
                    e = new Endereco();
                    e.setCdCliente(Integer.parseInt(tfCodigoCliente.getText()));
                    e.setLogradouroEnd(tfLogradouro.getText());
                    e.setBairroEnd(tfBairro.getText());
                    e.setNumeroEnd(tfNumero.getText());
                    e.setMunicipioEnd(tfMunicipio.getText());
                    e.setCepEnd(tfCep.getText());
                    e.setUfEnd("MG");
                    s.inserir(e);
                    cancelar();
                }

                break;
            case "editar":
                if (validar()) {
                    e = getEndereco();
                    e.setCdCliente(Integer.parseInt(tfCodigoCliente.getText()));
                    e.setLogradouroEnd(tfLogradouro.getText());
                    e.setBairroEnd(tfBairro.getText());
                    e.setNumeroEnd(tfNumero.getText());
                    e.setMunicipioEnd(tfMunicipio.getText());
                    e.setCepEnd(tfCep.getText());
                    e.setUfEnd("MG");
                    s.alterar(e);
                    cancelar();
                }
                break;
            case "visualizar":
                cancelar();
                break;
        }
    }

    private boolean validar() {
        String vazios = null;
        if (tfLogradouro.getText().length() < 2 || tfLogradouro.getText().length() > 50) {
            tfLogradouro.requestFocus();
            vazios = tfLogradouro.getPromptText();
        }
        if (tfNumero.getText().length() < 1 || tfNumero.getText().length() > 10) {
            if (vazios == null) {
                tfNumero.requestFocus();
                vazios = tfNumero.getPromptText();
            }
        }
        if (tfBairro.getText().length() < 1 || tfBairro.getText().length() > 45) {
            if (vazios == null) {
                tfBairro.requestFocus();
                vazios = tfBairro.getPromptText();
            }
        }
        if (tfMunicipio.getText().length() < 1 || tfMunicipio.getText().length() > 45) {
            if (vazios == null) {
                tfMunicipio.requestFocus();
                vazios = tfMunicipio.getPromptText();
            }
        }
        String cep = tfCep.getText().replace(" ", "");
        if (cep.length() < 9) {
            if (vazios == null) {
                tfCep.requestFocus();
                vazios = tfCep.getPromptText();
            }
        }
        return vazios == null;
    }

}
