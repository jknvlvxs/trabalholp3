/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class BuscarProdutosUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();
    Stage tela;
    @FXML
    private JFXButton btSelecionar;
    @FXML
    private JFXTextField tfPesquisa;
    @FXML
    TableView tbProduto;
    @FXML
    TableColumn<Produto, Integer> codigoCol;
    @FXML
    TableColumn<Produto, String> nomeCol;
    @FXML
    TableColumn<Produto, Double> precoCol;
    @FXML
    TableColumn<Produto, String> categoriaCol;
    @FXML
    TableColumn<Produto, Integer> quantidadeCol;

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdProduto"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmProduto"));
        precoCol.setCellValueFactory(new PropertyValueFactory<>("precoProduto"));
        categoriaCol.setCellValueFactory(new PropertyValueFactory<>("categoriaProduto"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantEstoque"));

        atualizarTabela();
    }

    private void atualizarTabela(List<Produto> produtos) {
        for (int i = 0; i < tbProduto.getItems().size(); i++) {
            tbProduto.getItems().clear();
        }
        for (Produto p : produtos) {
            tbProduto.getItems().add(p);
        }
    }

    private void atualizarTabela() {
        try {
            tbProduto.setItems(listaProduto());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Produto> listaProduto() throws DaoException {
        ObservableList<Produto> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosProduto().size(); i++) {
            list.add(s.consultarTodosProduto().get(i));
        }
        return list;
    }

    @FXML
    private void habilitarBotoes() {
        if (tbProduto.getSelectionModel().getSelectedItem() != null) {
            btSelecionar.setDisable(false);
        }
    }

    @FXML
    private void selecionar() {
        if (tbProduto.getSelectionModel().getSelectedItem() != null) {
            Produto p = (Produto) tbProduto.getSelectionModel().getSelectedItem();
            CrudVendasUIController cv = new CrudVendasUIController();
            cv.setProduto(p);
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Selecionar quantidade");
            dialog.setContentText("Insira a quantidade de " + p.getNmProduto() + " que deseja adicionar");

// Traditional way to get the response value.
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                cv.setQuantidade(Integer.parseInt(result.get()));
            }

            this.getTela().close();
        }
    }

    @FXML
    void pesquisar(KeyEvent event) throws DaoException, ControleException {
        if (event.getCode() == KeyCode.ENTER) {
            if (!"".equals(tfPesquisa.getText())) {
                try {
                    int codigo = Integer.parseInt(tfPesquisa.getText());
                    Produto p = s.consultarProdutoPorCod(codigo);
                    List<Produto> pList = new ArrayList();
                    pList.add(p);
                    atualizarTabela(pList);
                } catch (DaoException | NumberFormatException e) {
                    List<Produto> pList = s.filtroProduto(tfPesquisa.getText());
                    atualizarTabela(pList);
                }
            } else {
                atualizarTabela(listaProduto());
            }
        }
    }
}
