/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Venda;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class RelatorioVendasUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();

    @FXML
    JFXTextField tfValMin;
    @FXML
    JFXTextField tfValMax;
    @FXML
    JFXTextField tfStatus;
    @FXML
    JFXButton btPesquisar;
    @FXML
    JFXButton btGerar;
    @FXML
    TableView tbVenda;
    @FXML
    TableColumn<Venda, Date> dataCol;
    @FXML
    TableColumn<Venda, String> pagamentoCol;
    @FXML
    TableColumn<Venda, Integer> parcelasCol;
    @FXML
    TableColumn<Venda, String> statusCol;
    @FXML
    TableColumn<Venda, Double> freteCol;
    @FXML
    TableColumn<Venda, Double> compraCol;
    @FXML
    TableColumn<Venda, Double> totalCol;
    @FXML
    TableColumn<Venda, Integer> clienteCol;
    @FXML
    TableColumn<Venda, Integer> funcionarioCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dataCol.setCellValueFactory(new PropertyValueFactory<>("dtVenda"));
        pagamentoCol.setCellValueFactory(new PropertyValueFactory<>("formaPagamento"));
        parcelasCol.setCellValueFactory(new PropertyValueFactory<>("qtdParcelas"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("statusEntrega"));
        freteCol.setCellValueFactory(new PropertyValueFactory<>("precoFrete"));
        compraCol.setCellValueFactory(new PropertyValueFactory<>("precoCompra"));
        totalCol.setCellValueFactory(new PropertyValueFactory<>("precoTotal"));
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("cdCliente"));
        funcionarioCol.setCellValueFactory(new PropertyValueFactory<>("cdFuncionario"));
        atualizarTabela();
    }

    private void atualizarTabela(List<Venda> vendas) {
        for (int i = 0; i < tbVenda.getItems().size(); i++) {
            tbVenda.getItems().clear();
        }
        for (Venda v : vendas) {
            tbVenda.getItems().add(v);
        }
    }

    public void atualizarTabela() {
        try {
            tbVenda.setItems(listaVenda());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Venda> listaVenda() throws DaoException {
        ObservableList<Venda> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosVenda().size(); i++) {
            list.add(s.consultarTodosVenda().get(i));
        }
        return list;
    }

    @FXML
    private void clientes() {
        Programa.changeScene("clientes", "vendas", null);
    }

    @FXML
    private void produtos() {
        Programa.changeScene("produtos", "vendas", null);
    }

    @FXML
    private void funcionarios() {
        Programa.changeScene("funcionarios", "vendas", null);
    }

    @FXML
    private void voltar() {
        Programa.changeScene("menu", "vendas", null);
    }

    private List<Venda> getList() throws DaoException {
        List<Venda> all = s.consultarTodosVenda();
        List<Venda> vList = new ArrayList();

        //variaveis 
        Double vMin;
        Double vMax;
        String status;

        try {
            vMin = Double.parseDouble(tfValMin.getText());
        } catch (NumberFormatException e) {
            vMin = 0.0;
        }

        try {
            vMax = Double.parseDouble(tfValMax.getText());
        } catch (NumberFormatException e) {
            vMax = Double.POSITIVE_INFINITY;
        }

        for (int i = 0; i < all.size(); i++) {
            if (all.get(i).getPrecoTotal() >= vMin && all.get(i).getPrecoTotal() <= vMax) {
                if (tfStatus.getText().isEmpty() || all.get(i).getStatusEntrega().equals(tfStatus.getText())) {
                    vList.add(all.get(i));
                }
            }
        }
        return vList;
    }

    @FXML
    private void pesquisar() throws DaoException {
        Double vMin;
        Double vMax;
        String status;

        try {
            vMin = Double.parseDouble(tfValMin.getText());
        } catch (NumberFormatException e) {
            vMin = 0.0;
        }

        try {
            vMax = Double.parseDouble(tfValMax.getText());
        } catch (NumberFormatException e) {
            vMax = Double.POSITIVE_INFINITY;
        }
        atualizarTabela(s.relatorioVenda(vMin, vMax, tfStatus.getText()));
    }

    @FXML
    private void gerar() throws DaoException {
        Double vMin;
        Double vMax;
        String status;

        try {
            vMin = Double.parseDouble(tfValMin.getText());
        } catch (NumberFormatException e) {
            vMin = 0.0;
        }

        try {
            vMax = Double.parseDouble(tfValMax.getText());
        } catch (NumberFormatException e) {
            vMax = 99999999.0;
        }
        emitirRelatorio(s.relatorioVenda(vMin, vMax, tfStatus.getText()));
    }

    public void emitirRelatorio(List<Venda> vList) {
        String reportSource = "./RelatorioVendas.jasper";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("nomeEmpresa", "MercaDjin");

        try {
            JRBeanCollectionDataSource jrBean = new JRBeanCollectionDataSource(vList);
            JasperPrint jasperPrint = JasperFillManager.fillReport(reportSource, params, jrBean);

            JasperViewer.viewReport(jasperPrint, false);

        } catch (JRException ex) {
            ex.printStackTrace();
        }
    }
}
