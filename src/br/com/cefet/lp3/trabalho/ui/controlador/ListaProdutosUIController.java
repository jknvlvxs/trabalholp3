/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class ListaProdutosUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();

    @FXML
    private JFXButton btEditar;

    @FXML
    private JFXButton btExcluir;

    @FXML
    private JFXButton btVisualizar;
    @FXML
    private JFXButton btRelatorio;
    @FXML
    private JFXTextField tfPesquisa;
    @FXML
    TableView tbProduto;
    @FXML
    TableColumn<Produto, Integer> codigoCol;
    @FXML
    TableColumn<Produto, String> nomeCol;
    @FXML
    TableColumn<Produto, Double> precoCol;
    @FXML
    TableColumn<Produto, String> categoriaCol;
    @FXML
    TableColumn<Produto, Integer> quantidadeCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdProduto"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmProduto"));
        precoCol.setCellValueFactory(new PropertyValueFactory<>("precoProduto"));
        categoriaCol.setCellValueFactory(new PropertyValueFactory<>("categoriaProduto"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("quantEstoque"));

        atualizarTabela();
    }

    private void atualizarTabela(List<Produto> produtos) {
        for (int i = 0; i < tbProduto.getItems().size(); i++) {
            tbProduto.getItems().clear();
        }
        for (Produto p : produtos) {
            tbProduto.getItems().add(p);
        }
    }

    public void atualizarTabela() {
        try {
            tbProduto.setItems(listaProduto());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Produto> listaProduto() throws DaoException {
        ObservableList<Produto> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosProduto().size(); i++) {
            list.add(s.consultarTodosProduto().get(i));
        }
        return list;
    }

    //MENU
    @FXML
    private void clientes() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("clientes", "produtos", null);
    }

    @FXML
    private void vendas() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("vendas", "produtos", null);
    }

    @FXML
    private void funcionarios() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("funcionarios", "produtos", null);

    }

    @FXML
    private void voltar() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("menu", "produtos", null);
    }

    @FXML
    private void relatorio() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("rprodutos", "produtos", null);
    }

    // CRUD
    @FXML
    private void cadastrar() throws IOException, DaoException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudProdutosUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudProdutosUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Cadastrar produto");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("cadastrar", null);
        controller.setTela(stage);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void editar() throws IOException, DaoException {
        Produto p = (Produto) tbProduto.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudProdutosUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudProdutosUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Editar produto");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("editar", p);
        controller.setTela(stage);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void excluir() throws ControleException, DaoException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            Produto prod = (Produto) tbProduto.getSelectionModel().getSelectedItem();
            alert.setTitle("Excluir produto");
            alert.setHeaderText("Você tem certeza que excluir o produto " + prod.getNmProduto() + "?");
            alert.setContentText("Se tiver certeza, clique em OK para confirmar a exclusão");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                s.excluir(prod);
                atualizarTabela();
                tbProduto.autosize();
                desabilitarBotoes();
            } else {
                desabilitarBotoes();
            }
        } catch (Exception e) {
            desabilitarBotoes();
        }
    }

    @FXML
    private void visualizar() throws IOException, DaoException {
        Produto p = (Produto) tbProduto.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudProdutosUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudProdutosUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Visualizar produto");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("visualizar", p);
        controller.setTela(stage);
        stage.showAndWait();
    }

    // uteis
    @FXML
    private void habilitarBotoes() {
        if (tbProduto.getSelectionModel().getSelectedItem() != null) {
            btEditar.setDisable(false);
            btExcluir.setDisable(false);
            btVisualizar.setDisable(false);
        }
    }

    private void desabilitarBotoes() {
        btEditar.setDisable(true);
        btExcluir.setDisable(true);
        btVisualizar.setDisable(true);
    }

    @FXML
    void pesquisar(KeyEvent event) throws DaoException, ControleException {
        if (event.getCode() == KeyCode.ENTER) {
            if (!"".equals(tfPesquisa.getText())) {
                try {
                    int codigo = Integer.parseInt(tfPesquisa.getText());
                    Produto p = s.consultarProdutoPorCod(codigo);
                    List<Produto> pList = new ArrayList();
                    pList.add(p);
                    atualizarTabela(pList);
                } catch (DaoException | NumberFormatException e) {
                    List<Produto> pList = s.filtroProduto(tfPesquisa.getText());
                    atualizarTabela(pList);
                }
            } else {
                atualizarTabela(listaProduto());
            }
        }
    }
}
