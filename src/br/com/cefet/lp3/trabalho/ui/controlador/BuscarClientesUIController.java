/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class BuscarClientesUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();
    Stage tela;
    @FXML
    private JFXButton btSelecionar;
    @FXML
    private JFXTextField tfPesquisa;
    @FXML
    TableView tbCliente;
    @FXML
    TableColumn<Cliente, Integer> codigoCol;
    @FXML
    TableColumn<Cliente, String> nomeCol;
    @FXML
    TableColumn<Cliente, Double> cpfCol;
    @FXML
    TableColumn<Cliente, String> telefoneCol;
    @FXML
    TableColumn<Cliente, Integer> usuarioCol;
    @FXML
    TableColumn<Cliente, Integer> senhaCol;

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdCliente"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmCliente"));
        cpfCol.setCellValueFactory(new PropertyValueFactory<>("cpfCliente"));
        telefoneCol.setCellValueFactory(new PropertyValueFactory<>("telefoneCliente"));
        usuarioCol.setCellValueFactory(new PropertyValueFactory<>("usuarioCliente"));
        senhaCol.setCellValueFactory(new PropertyValueFactory<>("senhaCliente"));

        atualizarTabela();
    }

    private void atualizarTabela(List<Cliente> clientes) {
        for (int i = 0; i < tbCliente.getItems().size(); i++) {
            tbCliente.getItems().clear();
        }
        for (Cliente c : clientes) {
            tbCliente.getItems().add(c);
        }
    }

    private void atualizarTabela() {
        try {
            tbCliente.setItems(listaCliente());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Cliente> listaCliente() throws DaoException {
        ObservableList<Cliente> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosClientes().size(); i++) {
            list.add(s.consultarTodosClientes().get(i));
        }
        return list;
    }

    @FXML
    private void habilitarBotoes() {
        if (tbCliente.getSelectionModel().getSelectedItem() != null) {
            btSelecionar.setDisable(false);
        }
    }

    @FXML
    private void selecionar() {
        if (tbCliente.getSelectionModel().getSelectedItem() != null) {
            Cliente c = (Cliente) tbCliente.getSelectionModel().getSelectedItem();
            CrudVendasUIController cv = new CrudVendasUIController();
            cv.setCliente(c);
            this.getTela().close();
        }
    }

    @FXML
    void pesquisar(KeyEvent event) throws DaoException, ControleException {
        if (event.getCode() == KeyCode.ENTER) {
            if (!"".equals(tfPesquisa.getText())) {
                try {
                    int codigo = Integer.parseInt(tfPesquisa.getText());
                    Cliente c = s.consultarClientePorCod(codigo);
                    List<Cliente> cList = new ArrayList();
                    cList.add(c);
                    atualizarTabela(cList);
                } catch (DaoException | NumberFormatException e) {
                    List<Cliente> cList = s.filtroCliente(tfPesquisa.getText());
                    atualizarTabela(cList);
                }
            } else {
                atualizarTabela(listaCliente());
            }
        }
    }
}
