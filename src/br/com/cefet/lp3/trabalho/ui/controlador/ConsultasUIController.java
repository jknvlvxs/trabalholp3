/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.entidade.Venda;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class ConsultasUIController implements Initializable {

    @FXML
    private Label lbWelcome;
    @FXML
    private JFXTextField tfVendaC;
    @FXML
    private JFXTextField tfVendaD;
    @FXML
    private JFXTextField tfClienteC;
    @FXML
    private JFXTextField tfClienteD;
    @FXML
    private JFXTextField tfProdutoC;
    @FXML
    private JFXTextField tfProdutoD;
    @FXML
    private JFXTextField tfFuncionarioC;
    @FXML
    private JFXTextField tfFuncionarioD;

    Supermercado s = Supermercado.getInstance();

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void cliqueClientes() {
        Programa.changeScene("clientes", "consultas", null);
    }

    @FXML
    private void cliqueVendas() {
        Programa.changeScene("vendas", "consultas", null);
    }

    @FXML
    private void cliqueProdutos() {
        Programa.changeScene("produtos", "consultas", null);
    }

    @FXML
    private void cliqueFuncionarios() {
        String cod = "" + s.getFuncionarioLogado().getUsuarioFuncionario().charAt(0);
        if ("A".equals(cod) || "G".equals(cod)) {
            Programa.changeScene("funcionarios", "consultas", null);
        } else {
            Alert alerta = new Alert(Alert.AlertType.ERROR);
            alerta.setTitle("Acesso negado");
            alerta.setHeaderText("Você não tem permissão para esta ação");
            alerta.setContentText("É necessário ser gerente ou administrador para acessar esta janela");
            alerta.showAndWait();
        }
    }

    @FXML
    private void cliqueVoltar() {
        Programa.changeScene("menu", "consultas", null);
    }

    public void atualizarMsg() {
        lbWelcome.setText("Bem vindo, " + s.getFuncionarioLogado().getNmFuncionario());
    }

    public void atualizarCampos() {
        if (Programa.getObject() != null) {
            setarCampos(Programa.getObject(), Programa.getTelaAnterior());
        }
    }

    private void setarCampos(Object object, String telaAnterior) {
        // Object o = object;
        try {
            if (telaAnterior.equals("vendas")) {
                Venda v = (Venda) object;
                tfVendaC.setText(Integer.toString(v.getCdVenda()));
                tfVendaD.setText(Double.toString(v.getPrecoTotal()));
            }
            if (telaAnterior.equals("clientes")) {
                Cliente c = (Cliente) object;
                tfClienteC.setText(Integer.toString(c.getCdCliente()));
                tfClienteD.setText(c.getNmCliente());
            }
            if (telaAnterior.equals("produtos")) {
                Produto p = (Produto) object;
                tfProdutoC.setText(Integer.toString(p.getCdProduto()));
                tfProdutoD.setText(p.getNmProduto());
            }
            if (telaAnterior.equals("funcionarios")) {
                Funcionario f = (Funcionario) object;
                tfFuncionarioC.setText(Integer.toString(f.getCdFuncionario()));
                tfFuncionarioD.setText(f.getNmFuncionario());
            }
        } catch (Exception e) {
            System.out.println("fe na glock");
        }

    }
}
