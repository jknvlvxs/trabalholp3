/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author aluno
 */
public class LoginUIController implements Initializable {

    @FXML
    private Label lbResultado;

    @FXML
    private JFXPasswordField senha;

    @FXML
    private JFXTextField user;

    @FXML
    private JFXButton btEntrar;

    @FXML
    private ImageView imgLogo;

    @FXML
    private ImageView imgUser;

    @FXML
    private ImageView imgPswd;

    Supermercado s = Supermercado.getInstance();

    @FXML
    private void handleButtonAction(ActionEvent event) throws DaoException, IOException {
        if (verificarConta()) {
            Programa.changeScene("menu", "login", s.getFuncionarioLogado());
        } else {
            lbResultado.setText("Login inválido \nUsuário e/ou senha não existem");
//            AlersetHeaderText("Login inválido");
//            alerta.showAndWait();t alerta = new Alert(Alert.AlertType.ERROR);
//            alerta.setHeaderText("Login inválido");
//            alerta.showAndWait();
        }
    }

    @FXML
    private void alertaEsqueceu(ActionEvent event) {
        Alert alerta = new Alert(Alert.AlertType.ERROR);
        alerta.setTitle("Esqueci minha senha");
        alerta.setHeaderText("Bem vindo à redefinição de senha");
        alerta.setContentText("Para redefinir sua senha, contate um administrador ou gerente"
                + " e faça o pedido para alterar seus dados");
        alerta.showAndWait();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb
    ) {
        btEntrar.setDefaultButton(true);

//        File file = new File("imagens\\MercaDjin.png");
//        Image image = new Image(file.toURI().toString());
        Image img = new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm());
//        Image img = new Image(in);
        //input string
        imgLogo.setImage(img);

        img = new Image(getClass().getResource("imagens/usuario.png").toExternalForm());
        imgUser.setImage(img);

        img = new Image(getClass().getResource("imagens/senha.png").toExternalForm());
        imgPswd.setImage(img);
    }

    private boolean verificarConta() throws DaoException {
        List<Funcionario> f = s.consultarTodosFuncionario();
        for (int i = 0; i < f.size(); i++) {
            if (senha.getText().equals(f.get(i).getSenhaFuncionario()) && user.getText().equals(f.get(i).getUsuarioFuncionario())) {
                s.setFuncionarioLogado(f.get(i));
                return true;
            }
        }
        return false;
    }
}
