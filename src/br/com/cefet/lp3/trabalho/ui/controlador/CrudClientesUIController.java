/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.entidade.Endereco;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.util.Validacao;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author ADMIN
 */
public class CrudClientesUIController implements Initializable {

    Stage tela;
    Supermercado s = Supermercado.getInstance();
    private String origem;
    private Cliente cliente;

    @FXML
    private JFXTextField tfCodigo;
    @FXML
    private JFXTextField tfNome;
    @FXML
    private JFXTextField tfCpf;
    @FXML
    private JFXTextField tfTelefone;
    @FXML
    private JFXTextField tfUsuario;
    @FXML
    private JFXPasswordField tfSenha;
    @FXML
    private JFXButton btCancelar;
    @FXML
    private JFXButton btConfirmar;
    @FXML
    private JFXButton btNend;
    @FXML
    private JFXButton btBend;
    @FXML
    private JFXButton btRend;
    @FXML
    private Label lbTitulo;
    @FXML
    TableView tbEndereco;
    @FXML
    TableColumn<Endereco, String> logradouroCol;
    @FXML
    TableColumn<Endereco, String> numeroCol;
    @FXML
    TableColumn<Endereco, String> bairroCol;
    @FXML
    TableColumn<Endereco, String> municipioCol;
    @FXML
    TableColumn<Endereco, String> ufCol;
    @FXML
    TableColumn<Endereco, String> cepCol;

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        btConfirmar.setDefaultButton(true);
        logradouroCol.setCellValueFactory(new PropertyValueFactory<>("logradouroEnd"));
        numeroCol.setCellValueFactory(new PropertyValueFactory<>("numeroEnd"));
        bairroCol.setCellValueFactory(new PropertyValueFactory<>("bairroEnd"));
        municipioCol.setCellValueFactory(new PropertyValueFactory<>("municipioEnd"));
        ufCol.setCellValueFactory(new PropertyValueFactory<>("ufEnd"));
        cepCol.setCellValueFactory(new PropertyValueFactory<>("cepEnd"));
    }

    private void atualizarTabela() {
        try {
            tbEndereco.setItems(listaEndereco());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Endereco> listaEndereco() throws DaoException {
        ObservableList<Endereco> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarEnderecoPorCodCliente(getCliente().getCdCliente()).size(); i++) {
            list.add(s.consultarEnderecoPorCodCliente(getCliente().getCdCliente()).get(i));
        }
        return list;
    }

    public void ajustarTela(String origem, Cliente c) throws DaoException {
        setOrigem(origem);
        switch (origem) {
            case "cadastrar":
                btNend.setDisable(true);
                btBend.setDisable(true);
                btRend.setDisable(true);
                tfCodigo.setText(Integer.toString(s.getProximoCodigoCliente()));
                break;
            case "editar":
                setCliente(c);
                atualizarTabela();
                lbTitulo.setText("EDIÇÃO DE CLIENTE");
                tfCodigo.setText(Integer.toString(c.getCdCliente()));
                tfNome.setText(c.getNmCliente());
                tfCpf.setText(c.getCpfCliente());
                tfTelefone.setText(c.getTelCliente());
                tfUsuario.setText(c.getUsuarioCliente());
                tfSenha.setText(c.getSenhaCliente());
                break;
            case "visualizar":
                setCliente(c);
                atualizarTabela();
                lbTitulo.setText("VISUALIZAÇÃO DE CLIENTE");
                btCancelar.setVisible(false);
                btConfirmar.setText("Fechar");
                tfNome.setDisable(true);
                tfCpf.setDisable(true);
                tfTelefone.setDisable(true);
                tfUsuario.setDisable(true);
                tfSenha.setDisable(true);
                btNend.setDisable(true);
                btBend.setDisable(true);
                btRend.setDisable(true);
                tfCodigo.setText(Integer.toString(c.getCdCliente()));
                tfNome.setText(c.getNmCliente());
                tfCpf.setText(c.getCpfCliente());
                tfTelefone.setText(c.getTelCliente());
                tfUsuario.setText(c.getUsuarioCliente());
                tfSenha.setText(c.getSenhaCliente());
        }
    }

    @FXML
    private void cancelar() {
        this.getTela().close();
    }

    @FXML
    private void confirmar() throws ControleException, DaoException, IOException {
        Cliente c;
        switch (origem) {
            case "cadastrar":
                if (validar()) {
                    c = new Cliente();
                    c.setNmCliente(tfNome.getText());
                    c.setCpfCliente(tfCpf.getText());
                    c.setTelCliente(tfTelefone.getText());
                    c.setUsuarioCliente(tfUsuario.getText());
                    c.setSenhaCliente(tfSenha.getText());
                    c.setCdCliente(Integer.parseInt(tfCodigo.getText()));
                    s.inserir(c);
                    setCliente(c);
                    novo();
                    cancelar();
                }
                break;

            case "editar":
                if (validar()) {
                    c = getCliente();
                    c.setNmCliente(tfNome.getText());
                    c.setCpfCliente(tfCpf.getText());
                    c.setTelCliente(tfTelefone.getText());
                    c.setUsuarioCliente(tfUsuario.getText());
                    c.setSenhaCliente(tfSenha.getText());
                    s.alterar(c);
                    if (tbEndereco.getSelectionModel().getSelectedItem() != null) {
                        Endereco e = (Endereco) tbEndereco.getSelectionModel().getSelectedItem();
                        s.excluir(e);
                        atualizarTabela();
                    }
                    cancelar();
                }
                break;

            case "visualizar":
                cancelar();
                break;
        }
    }

    @FXML
    private void cliqueTabela(MouseEvent event) throws IOException, DaoException {
        if (tbEndereco.getSelectionModel().getSelectedItem() != null) {
            if (!origem.equals("visualizar")) {
                btRend.setDisable(false);
            }
            if (event.getClickCount() > 1) {
                Endereco e = (Endereco) tbEndereco.getSelectionModel().getSelectedItem();
                desabilitarBotoes();
                FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudEnderecosUI.fxml"));
                Parent fxml1 = loader1.load();
                Scene scene1 = new Scene(fxml1);
                CrudEnderecosUIController controller = loader1.getController();
                Stage stage1 = new Stage();
                stage1.setScene(scene1);
                stage1.centerOnScreen();
                stage1.setMinHeight(600);
                stage1.setMinWidth(750);
                stage1.setResizable(false);
                stage1.setTitle("Visualizar endereco");
                stage1.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
                controller.ajustarTela("visualizar", e, getCliente());
                controller.setTela(stage1);
                stage1.initModality(Modality.APPLICATION_MODAL);
                stage1.showAndWait();
            }
        }
    }

    @FXML
    private void novo() throws IOException, DaoException {
        desabilitarBotoes();
        FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudEnderecosUI.fxml"));
        Parent fxml1 = loader1.load();
        Scene scene1 = new Scene(fxml1);
        CrudEnderecosUIController controller = loader1.getController();
        Stage stage1 = new Stage();
        stage1.setScene(scene1);
        stage1.centerOnScreen();
        stage1.setMinHeight(600);
        stage1.setMinWidth(750);
        stage1.setResizable(false);
        stage1.setTitle("Cadastrar endereco");
        stage1.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("cadastrar", null, getCliente());
        controller.setTela(stage1);
        stage1.initModality(Modality.APPLICATION_MODAL);
        stage1.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void remover() throws DaoException, ControleException {
        if (tbEndereco.getSelectionModel().getSelectedItem() != null) {;
            Endereco e = (Endereco) tbEndereco.getSelectionModel().getSelectedItem();
            s.excluir(e);
            atualizarTabela();
        }
    }

    private boolean validar() throws DaoException {
        String vazios = null;
        if (tfNome.getText().length() < 2 || tfNome.getText().length() > 50) {
            tfNome.requestFocus();
            vazios = tfNome.getPromptText();
        }
        if (!Validacao.validaCPF(tfCpf.getText()) || !Validacao.CPFExistente(tfCpf.getText(), "inserir")) {
            if (vazios == null) {
                tfCpf.requestFocus();
                vazios = tfCpf.getPromptText();
            }
        }
        String tel = tfTelefone.getText().replace(" ", "");
        if (tel.length() < 13) {
            if (vazios == null) {
                tfTelefone.requestFocus();
                vazios = tfTelefone.getPromptText();
            }
        }
        if (tfTelefone.getText().length() < 13 || tfTelefone.getText().length() > 14) {
            if (vazios == null) {
                tfTelefone.requestFocus();
                vazios = tfTelefone.getPromptText();
            }
        }
        if (tfUsuario.getText().length() <= 1 || tfUsuario.getText().length() > 100) {
            if (vazios == null) {
                tfUsuario.requestFocus();
                vazios += " " + tfUsuario.getPromptText();
            }
        }
        if (tfSenha.getText().length() <= 1 || tfSenha.getText().length() > 16) {
            if (vazios == null) {
                tfSenha.requestFocus();
                vazios += " " + tfSenha.getPromptText();
            }
        }
        return vazios == null;

    }

    private void desabilitarBotoes() {
        //btConfirmar.setDisable(true);
        //btNend.setDisable(true);
        //btBend.setDisable(true);
        btRend.setDisable(true);
    }

}
