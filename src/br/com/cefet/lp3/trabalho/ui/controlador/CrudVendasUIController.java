/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.entidade.Venda;
import br.com.cefet.lp3.trabalho.entidade.VendaProduto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import java.util.List;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author ADMIN
 */
public class CrudVendasUIController implements Initializable {

    Stage tela;
    Supermercado s = Supermercado.getInstance();
    private String origem;
    private Venda venda;
    private static Cliente cliente;
    private static Funcionario funcionario;
    private static Produto produto;
    private static int quantidade;
    @FXML
    TableView tbProduto;
    @FXML
    TableView tbVProduto;
    @FXML
    TableColumn<Produto, String> codigoCol;
    @FXML
    TableColumn<Produto, String> nomeCol;
    @FXML
    TableColumn<Produto, Double> precoCol;
    @FXML
    TableColumn<VendaProduto, Double> unicoCol;
    @FXML
    TableColumn<VendaProduto, Integer> quantidadeCol;
    @FXML
    private JFXTextField tfCodigo;
    @FXML
    private JFXTextField tfData;
    @FXML
    private JFXTextField tfFormaPag;
    @FXML
    private JFXTextField tfQuantidade;
    @FXML
    private JFXTextField tfStatus;
    @FXML
    private JFXTextField tfFrete;
    @FXML
    private JFXTextField tfCompra;
    @FXML
    private JFXTextField tfTotal;
    @FXML
    private JFXTextField tfCliente;
    @FXML
    private JFXTextField tfFuncionario;
    @FXML
    private JFXButton btCancelar;
    @FXML
    private JFXButton btConfirmar;
    @FXML
    private JFXButton btBuscar;
    @FXML
    private JFXButton btRemover;

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public Venda getVenda() {
        return venda;
    }

    public void setVenda(Venda venda) {
        this.venda = venda;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btConfirmar.setDefaultButton(true);
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdProduto"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmProduto"));
        unicoCol.setCellValueFactory(new PropertyValueFactory<>("precoProduto"));
        precoCol.setCellValueFactory(new PropertyValueFactory<>("precoProdutoVenda"));
        quantidadeCol.setCellValueFactory(new PropertyValueFactory<>("qtdProdutoVenda"));
    }

    private void atualizarTabela() {
        try {
            tbProduto.setItems(listaproduto());
            tbVProduto.setItems(listav());

        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Produto> listaproduto() throws DaoException {
        ObservableList<Produto> list;
        list = FXCollections.observableArrayList();
        List<VendaProduto> listVp = s.consultarVendaProdutoPorCodVenda(getVenda().getCdVenda());

        for (int i = 0; i < listVp.size(); i++) {
            list.add(s.consultarProdutoPorCod(listVp.get(i).getCdProduto()));
        }
        return list;
    }

    private ObservableList<VendaProduto> listav() throws DaoException {
        ObservableList<VendaProduto> list;
        list = FXCollections.observableArrayList();
        List<VendaProduto> listVp = s.consultarVendaProdutoPorCodVenda(getVenda().getCdVenda());

        for (int i = 0; i < listVp.size(); i++) {
            list.add(listVp.get(i));
        }
        return list;
    }

    public void ajustarTela(String origem, Venda v) throws DaoException {
        setVenda(v);
        setOrigem(origem);
        switch (origem) {
            case "cadastrar":
                tfCodigo.setText(Integer.toString(s.getProximoCodigoVenda()));
                Long longData = System.currentTimeMillis();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                String stringData = sdf.format(longData);
                tfData.setText(stringData);
                break;
            case "editar":
                atualizarTabela();
                tfCliente.setDisable(true);
                tfFuncionario.setDisable(true);
                tfCodigo.setText(Integer.toString(v.getCdVenda()));
                tfData.setText(v.getDtVenda().toString());
                tfFormaPag.setText(v.getFormaPagamento());
                tfQuantidade.setText(Integer.toString(v.getQtdParcelas()));
                tfStatus.setText(v.getStatusEntrega());
                tfFrete.setText(Double.toString(v.getPrecoFrete()));
                tfCompra.setText(Double.toString(v.getPrecoCompra()));
                tfTotal.setText(Double.toString(v.getPrecoCompra() + v.getPrecoFrete()));
                try {
                    setCliente(s.consultarClientePorCod(v.getCdCliente()));
                    setFuncionario(s.consultarFuncionarioPorCod(v.getCdFuncionario()));
                } catch (NullPointerException e) {
                }
                try {
                    tfCliente.setText(s.consultarClientePorCod(v.getCdCliente()).getNmCliente());
                } catch (NullPointerException e) {
                }

                try {
                    tfFuncionario.setText(s.consultarFuncionarioPorCod(v.getCdFuncionario()).getNmFuncionario());
                } catch (NullPointerException e) {
                }
                break;
            case "visualizar":
                atualizarTabela();
                tfCodigo.setDisable(true);
                tfData.setDisable(true);
                tfFormaPag.setDisable(true);
                tfQuantidade.setDisable(true);
                tfStatus.setDisable(true);
                tfFrete.setDisable(true);
                tfCompra.setDisable(true);
                tfTotal.setDisable(true);
                tfCliente.setDisable(true);
                tfFuncionario.setDisable(true);
                tfCodigo.setText(Integer.toString(v.getCdVenda()));
                tfData.setText(v.getDtVenda().toString());
                tfFormaPag.setText(v.getFormaPagamento());
                tfQuantidade.setText(Integer.toString(v.getQtdParcelas()));
                tfStatus.setText(v.getStatusEntrega());
                tfFrete.setText(Double.toString(v.getPrecoFrete()));
                tfCompra.setText(Double.toString(v.getPrecoCompra()));
                tfTotal.setText(Double.toString(v.getPrecoTotal()));
                try {
                    tfCliente.setText(s.consultarClientePorCod(v.getCdCliente()).getNmCliente());
                } catch (NullPointerException e) {
                }

                try {
                    tfFuncionario.setText(s.consultarFuncionarioPorCod(v.getCdFuncionario()).getNmFuncionario());
                } catch (NullPointerException e) {
                }
                btCancelar.setVisible(false);
                btConfirmar.setText("Fechar");
                btBuscar.setDisable(true);
                break;

        }
    }

    @FXML
    private void confirmar() throws ParseException, DaoException, ControleException, IOException {
        Venda v;
        switch (origem) {
            case "cadastrar":
                v = new Venda();
                if (validar()) {

                    Long longData = System.currentTimeMillis();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    String stringData = sdf.format(longData);
                    Date rdf = null;
                    try {
                        rdf = sdf.parse(stringData);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    v.setCdVenda(Integer.parseInt(tfCodigo.getText()));
                    v.setDtVenda(rdf);
                    v.setFormaPagamento(tfFormaPag.getText());
                    try {
                        v.setQtdParcelas(Integer.parseInt(tfQuantidade.getText()));
                    } catch (NumberFormatException e) {

                    }
                    v.setStatusEntrega(tfStatus.getText());
                    try {
                        v.setPrecoFrete(Double.parseDouble(tfFrete.getText()));
                    } catch (NumberFormatException e) {

                    }
                    try {
                        v.setPrecoCompra((Double.parseDouble(tfCompra.getText())));
                    } catch (NumberFormatException e) {

                    }
                   
                    v.setPrecoTotal(v.getPrecoCompra() + v.getPrecoFrete());

                    try {
                        v.setCdCliente(getCliente().getCdCliente());
                    } catch (NullPointerException e) {
                        v.setCdCliente(-1);

                    }

                    try {
                        v.setCdFuncionario(getFuncionario().getCdFuncionario());

                    } catch (NullPointerException e) {
                        v.setCdFuncionario(-1);
                    }

                    setVenda(v);
                }
                try {
                    List<VendaProduto> vp = s.consultarVendaProdutoPorCodVenda(v.getCdVenda());
                    if (vp.isEmpty()) {
                        s.inserir(v);
                        buscar();
                    } else {
                        cancelar();
                    }
                } catch (NullPointerException e) {

                }

                break;
            case "editar":
                if (validar()) {
                    v = getVenda();
                    v.setFormaPagamento(tfFormaPag.getText());
                    v.setQtdParcelas(Integer.parseInt(tfQuantidade.getText()));
                    v.setStatusEntrega(tfStatus.getText());
                    v.setPrecoFrete(Double.parseDouble(tfFrete.getText()));
                    v.setPrecoCompra(Double.parseDouble(tfCompra.getText()));
                    v.setPrecoTotal(Double.parseDouble(tfTotal.getText()));
                    s.alterar(v);
                    cancelar();
                }
                break;
            case "visualizar":
                cancelar();
                break;

        }
    }

    @FXML
    private void atualizarpreco() throws DaoException {
        Venda v = getVenda();

        try {
            List<VendaProduto> vp = s.consultarVendaProdutoPorCodVenda(v.getCdVenda());
            double preco = 0;
            for (int i = 0; i < vp.size(); i++) {
                preco += vp.get(i).getPrecoProdutoVenda();
            }
            v.setPrecoCompra(preco);
            tfCompra.setText(Double.toString(preco));
            v.setPrecoFrete(Double.parseDouble(tfFrete.getText()));
            v.setPrecoTotal(Double.parseDouble(tfFrete.getText()) + Double.parseDouble(tfCompra.getText()));
            tfTotal.setText(Double.toString(v.getPrecoTotal()));
        } catch (NumberFormatException | NullPointerException e) {

        }

    }

    @FXML
    private void cancelar() {
        this.getTela().close();
    }

    @FXML
    private void buscar() throws IOException, ControleException, DaoException {
        FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/BuscarProdutosUI.fxml"));
        Parent fxml1 = loader1.load();
        Scene scene1 = new Scene(fxml1);
        BuscarProdutosUIController controller = loader1.getController();
        Stage stage1 = new Stage();
        stage1.setScene(scene1);
        stage1.centerOnScreen();
        stage1.setMinHeight(600);
        stage1.setMinWidth(750);
        stage1.setResizable(false);
        stage1.setTitle("Buscar produto");
        controller.setTela(stage1);
        stage1.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        stage1.initModality(Modality.APPLICATION_MODAL);
        stage1.showAndWait();

        try {
            VendaProduto vp = new VendaProduto();
            vp.setCdProduto(getProduto().getCdProduto());
            vp.setCdVenda(getVenda().getCdVenda());
            vp.setQtdProdutoVenda(getQuantidade());
            vp.setPrecoProdutoVenda(getQuantidade() * getProduto().getPrecoProduto());
            s.inserir(vp);
            Venda v = getVenda();
            v.setPrecoCompra(vp.getPrecoProdutoVenda());
            v.setPrecoTotal(v.getPrecoFrete()+vp.getPrecoProdutoVenda());
            s.alterar(v);
            Produto p = s.consultarProdutoPorCod(vp.getCdProduto());
            p.setQuantEstoque(p.getQuantEstoque() - vp.getQtdProdutoVenda());
            s.alterar(p);
            ListaProdutosUIController lp = new ListaProdutosUIController();
            lp.atualizarTabela();
        } catch (Exception e) {

        }

        atualizarpreco();
        atualizarTabela();
    }

    @FXML
    private void remover() throws DaoException, ControleException {
        if (tbProduto.getSelectionModel().getSelectedItem() != null || tbVProduto.getSelectionModel().getSelectedItem() != null) {
            Produto p = null;
            if (tbProduto.getSelectionModel().getSelectedItem() != null) {
                p = (Produto) tbProduto.getSelectionModel().getSelectedItem();
            }
            if (tbVProduto.getSelectionModel().getSelectedItem() != null) {
                VendaProduto vp = (VendaProduto) tbVProduto.getSelectionModel().getSelectedItem();
                p = s.consultarProdutoPorCod(vp.getCdProduto());
            }
            List<VendaProduto> vendap = s.consultarVendaProdutoPorCodProduto(p.getCdProduto());

            for (int i = 0; i < vendap.size(); i++) {
                if (vendap.get(i).getCdVenda() == getVenda().getCdVenda()) {
                    s.excluir(vendap.get(i));
                }
            }
            atualizarTabela();
            atualizarpreco();
        }
    }

    @FXML
    private void cliqueTabela(MouseEvent event) throws IOException, DaoException {
        if (tbProduto.getSelectionModel().getSelectedItem() != null || tbVProduto.getSelectionModel().getSelectedItem() != null) {
            if (!origem.equals("visualizar")) {
                btRemover.setDisable(false);
            }
            if (event.getClickCount() > 1) {
                Produto p = null;
                if (tbProduto.getSelectionModel().getSelectedItem() != null) {
                    p = (Produto) tbProduto.getSelectionModel().getSelectedItem();
                }
                if (tbVProduto.getSelectionModel().getSelectedItem() != null) {
                    VendaProduto vp = (VendaProduto) tbVProduto.getSelectionModel().getSelectedItem();
                    p = s.consultarProdutoPorCod(vp.getCdProduto());
                }
                FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudProdutosUI.fxml"));
                Parent fxml1 = loader1.load();
                Scene scene1 = new Scene(fxml1);
                CrudProdutosUIController controller = loader1.getController();
                Stage stage1 = new Stage();
                stage1.setScene(scene1);
                stage1.centerOnScreen();
                stage1.setMinHeight(600);
                stage1.setMinWidth(750);
                stage1.setResizable(false);
                stage1.setTitle("Visualizar produto");
                stage1.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
                controller.ajustarTela("visualizar", p);
                controller.setTela(stage1);
                stage1.initModality(Modality.APPLICATION_MODAL);
                stage1.showAndWait();
            }
        }
    }

    @FXML
    private void buscarCliente() throws IOException {
        FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/BuscarClientesUI.fxml"));
        Parent fxml1 = loader1.load();
        Scene scene1 = new Scene(fxml1);
        BuscarClientesUIController controller = loader1.getController();
        Stage stage1 = new Stage();
        stage1.setScene(scene1);
        stage1.centerOnScreen();
        stage1.setMinHeight(600);
        stage1.setMinWidth(750);
        stage1.setResizable(false);
        stage1.setTitle("Selecionar cliente");
        stage1.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.setTela(stage1);
        stage1.initModality(Modality.APPLICATION_MODAL);
        stage1.showAndWait();
        try {
            tfCliente.setText(getCliente().getNmCliente());
        } catch (Exception e) {

        }
    }

    @FXML
    private void buscarFuncionario() throws IOException {
        FXMLLoader loader1 = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/BuscarFuncionariosUI.fxml"));
        Parent fxml1 = loader1.load();
        Scene scene1 = new Scene(fxml1);
        BuscarFuncionariosUIController controller = loader1.getController();
        Stage stage1 = new Stage();
        stage1.setScene(scene1);
        stage1.centerOnScreen();
        stage1.setMinHeight(600);
        stage1.setMinWidth(750);
        stage1.setResizable(false);
        stage1.setTitle("Selecionar funcionario");
        stage1.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.setTela(stage1);
        stage1.initModality(Modality.APPLICATION_MODAL);
        stage1.showAndWait();
        try {
            tfFuncionario.setText(getFuncionario().getNmFuncionario());
        } catch (Exception e) {

        }
    }

    private boolean validar() throws IOException, ControleException, DaoException {
        String vazios = null;
        if (tfFormaPag.getText().length() < 2 || tfFormaPag.getText().length() > 20) {
            tfFormaPag.requestFocus();
            vazios = tfFormaPag.getPromptText();
        }

        try {
            Integer.parseInt(tfQuantidade.getText());
        } catch (NumberFormatException e) {
            if (vazios == null) {
                tfQuantidade.requestFocus();
                vazios += " " + tfQuantidade.getPromptText();
            }
        }

        if (tfStatus.getText().length() <= 1 || tfStatus.getText().length() > 10) {
            if (vazios == null) {
                tfStatus.requestFocus();
                vazios += " " + tfStatus.getPromptText();
            }
        }

//        if (tbProduto.getItems().size() <= 0) {
//            if (vazios == null) {
//                buscar();
//            }
//        }
            try {
                Double.parseDouble(tfFrete.getText());
            } catch (NumberFormatException e) {
                if (vazios == null) {
                    tfFrete.requestFocus();
                    vazios += " " + tfFrete.getPromptText();
                }
            }

            return vazios == null;
        }

    }
