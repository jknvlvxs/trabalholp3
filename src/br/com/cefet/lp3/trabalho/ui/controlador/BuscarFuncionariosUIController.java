/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class BuscarFuncionariosUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();
    Stage tela;
    @FXML
    private JFXButton btSelecionar;
    @FXML
    private JFXTextField tfPesquisa;
    @FXML
    TableView tbFuncionario;
    @FXML
    TableColumn<Funcionario, Integer> codigoCol;
    @FXML
    TableColumn<Funcionario, String> nomeCol;
    @FXML
    TableColumn<Funcionario, String> usuarioCol;
    @FXML
    TableColumn<Funcionario, String> senhaCol;

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdFuncionario"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmFuncionario"));
        usuarioCol.setCellValueFactory(new PropertyValueFactory<>("usuarioFuncionario"));
        senhaCol.setCellValueFactory(new PropertyValueFactory<>("senhaFuncionario"));

        atualizarTabela();
    }

    private void atualizarTabela(List<Funcionario> funcionarios) {
        for (int i = 0; i < tbFuncionario.getItems().size(); i++) {
            tbFuncionario.getItems().clear();
        }
        for (Funcionario f : funcionarios) {
            tbFuncionario.getItems().add(f);
        }
    }

    private void atualizarTabela() {
        try {
            tbFuncionario.setItems(listaFuncionario());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Funcionario> listaFuncionario() throws DaoException {
        ObservableList<Funcionario> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosFuncionario().size(); i++) {
            list.add(s.consultarTodosFuncionario().get(i));
        }
        return list;
    }

    @FXML
    private void habilitarBotoes() {
        if (tbFuncionario.getSelectionModel().getSelectedItem() != null) {
            btSelecionar.setDisable(false);
        }
    }

    @FXML
    private void selecionar() {
        if (tbFuncionario.getSelectionModel().getSelectedItem() != null) {
            Funcionario f = (Funcionario) tbFuncionario.getSelectionModel().getSelectedItem();
            CrudVendasUIController cv = new CrudVendasUIController();
            cv.setFuncionario(f);

            this.getTela().close();
        }
    }

    @FXML
    void pesquisar(KeyEvent event) throws DaoException, ControleException {
        if (event.getCode() == KeyCode.ENTER) {
            if (!"".equals(tfPesquisa.getText())) {
                try {
                    int codigo = Integer.parseInt(tfPesquisa.getText());
                    Funcionario f = s.consultarFuncionarioPorCod(codigo);
                    List<Funcionario> fList = new ArrayList();
                    fList.add(f);
                    atualizarTabela(fList);
                } catch (DaoException | NumberFormatException e) {
                    List<Funcionario> fList = s.filtroFuncionario(tfPesquisa.getText());
                    atualizarTabela(fList);
                }
            } else {
                atualizarTabela(listaFuncionario());
            }
        }
    }
}
