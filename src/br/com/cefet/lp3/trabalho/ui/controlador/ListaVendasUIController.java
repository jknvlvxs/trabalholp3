/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Venda;
import br.com.cefet.lp3.trabalho.entidade.VendaProduto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class ListaVendasUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();
    @FXML
    private JFXButton btEditar;
    @FXML
    private JFXButton btExcluir;
    @FXML
    private JFXButton btVisualizar;
    @FXML
    private JFXButton btRelatorio;
    @FXML
    private JFXButton btRelatorio2;
    @FXML
    TableView tbVenda;
    @FXML
    private JFXTextField tfPesquisa;
    @FXML
    TableColumn<Venda, Date> dataCol;
    @FXML
    TableColumn<Venda, String> pagamentoCol;
    @FXML
    TableColumn<Venda, Integer> parcelasCol;
    @FXML
    TableColumn<Venda, String> statusCol;
    @FXML
    TableColumn<Venda, Double> freteCol;
    @FXML
    TableColumn<Venda, Double> compraCol;
    @FXML
    TableColumn<Venda, Double> totalCol;
    @FXML
    TableColumn<Venda, Integer> clienteCol;
    @FXML
    TableColumn<Venda, Integer> funcionarioCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dataCol.setCellValueFactory(new PropertyValueFactory<>("dtVenda"));
        pagamentoCol.setCellValueFactory(new PropertyValueFactory<>("formaPagamento"));
        parcelasCol.setCellValueFactory(new PropertyValueFactory<>("qtdParcelas"));
        statusCol.setCellValueFactory(new PropertyValueFactory<>("statusEntrega"));
        freteCol.setCellValueFactory(new PropertyValueFactory<>("precoFrete"));
        compraCol.setCellValueFactory(new PropertyValueFactory<>("precoCompra"));
        totalCol.setCellValueFactory(new PropertyValueFactory<>("precoTotal"));
        clienteCol.setCellValueFactory(new PropertyValueFactory<>("cdCliente"));
        funcionarioCol.setCellValueFactory(new PropertyValueFactory<>("cdFuncionario"));
        atualizarTabela();
    }

    private void atualizarTabela() {
        try {
            tbVenda.setItems(listaVenda());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Venda> listaVenda() throws DaoException {
        ObservableList<Venda> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosVenda().size(); i++) {
            list.add(s.consultarTodosVenda().get(i));
        }
        return list;
    }

    @FXML
    private void clientes() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("clientes", "vendas", null);
    }

    @FXML
    private void produtos() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("produtos", "vendas", null);
    }

    @FXML
    private void funcionarios() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("funcionarios", "vendas", null);
    }

    @FXML
    private void voltar() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("menu", "vendas", null);
    }

    @FXML
    private void relatorio() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("rvendas", "vendas", null);
    }

    @FXML
    private void relatorio2() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("rprodutovendas", "vendas", null);
    }

    //CRUD
    @FXML
    private void cadastrar() throws IOException, DaoException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudVendasUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudVendasUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Cadastrar venda");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("cadastrar", null);
        controller.setTela(stage);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void editar() throws IOException, DaoException {
        Venda v = (Venda) tbVenda.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudVendasUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudVendasUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Editar venda");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("editar", v);
        controller.setTela(stage);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void excluir() throws DaoException, ControleException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            Venda v = (Venda) tbVenda.getSelectionModel().getSelectedItem();
            alert.setTitle("Excluir venda");
            alert.setHeaderText("Você tem certeza que excluir a venda " + v.getCdVenda() + "?");
            alert.setContentText("Se tiver certeza, clique em OK para confirmar a exclusão");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                List<VendaProduto> vp = s.consultarVendaProdutoPorCodVenda(v.getCdVenda());
                s.excluir(v);
                for (int i = 0; i < vp.size(); i++) {
                    s.excluir(vp.get(i));
                }
                atualizarTabela();
                desabilitarBotoes();
            } else {
                desabilitarBotoes();
            }
        } catch (Exception e) {
            desabilitarBotoes();
        }
    }

    @FXML
    private void visualizar() throws IOException, DaoException {
        Venda v = (Venda) tbVenda.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudVendasUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudVendasUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setMinHeight(700);
        stage.setMinWidth(900);
        stage.setResizable(false);
        stage.setTitle("Visualizar venda");
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("visualizar", v);
        controller.setTela(stage);
        stage.showAndWait();
    }

    //UTIL
    @FXML
    private void habilitarBotoes() {
        if (tbVenda.getSelectionModel().getSelectedItem() != null) {
            btEditar.setDisable(false);
            btExcluir.setDisable(false);
            btVisualizar.setDisable(false);
        }
    }

    private void desabilitarBotoes() {
        btEditar.setDisable(true);
        btExcluir.setDisable(true);
        btVisualizar.setDisable(true);
    }

    @FXML
    private void pesquisar() {

    }
}
