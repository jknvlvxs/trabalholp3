/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class ListaFuncionariosUIController implements Initializable {

    @FXML
    private Label lbWelcome;

    Supermercado s = Supermercado.getInstance();
    @FXML
    private JFXButton btEditar;
    @FXML
    private JFXButton btExcluir;
    @FXML
    private JFXButton btVisualizar;
    @FXML
    private JFXTextField tfPesquisa;
    @FXML
    TableView tbFuncionario;
    @FXML
    TableColumn<Funcionario, Integer> codigoCol;
    @FXML
    TableColumn<Funcionario, String> nomeCol;
    @FXML
    TableColumn<Funcionario, String> usuarioCol;
    @FXML
    TableColumn<Funcionario, String> senhaCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdFuncionario"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmFuncionario"));
        usuarioCol.setCellValueFactory(new PropertyValueFactory<>("usuarioFuncionario"));
        senhaCol.setCellValueFactory(new PropertyValueFactory<>("senhaFuncionario"));

        atualizarTabela();
    }

    private void atualizarTabela(List<Funcionario> funcionarios) {
        for (int i = 0; i < tbFuncionario.getItems().size(); i++) {
            tbFuncionario.getItems().clear();
        }
        for (Funcionario f : funcionarios) {
            tbFuncionario.getItems().add(f);
        }
    }

    public void atualizarTabela() {
        try {
            tbFuncionario.setItems(listaFuncionario());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Funcionario> listaFuncionario() throws DaoException {
        ObservableList<Funcionario> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosFuncionario().size(); i++) {
            list.add(s.consultarTodosFuncionario().get(i));
        }
        return list;
    }

    @FXML
    private void clientes() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("clientes", "funcionarios", null);
    }

    @FXML
    private void vendas() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("vendas", "funcionarios", null);
    }

    @FXML
    private void produtos() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("produtos", "funcionarios", null);
    }

    @FXML
    private void voltar() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("menu", "funcionarios", null);
    }

    //CRUD
    @FXML
    private void cadastrar() throws IOException, DaoException {
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudFuncionariosUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudFuncionariosUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Cadastrar funcionario");
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("cadastrar", null);
        controller.setTela(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void editar() throws IOException, DaoException {
        Funcionario f = (Funcionario) tbFuncionario.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudFuncionariosUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudFuncionariosUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Editar funcionario");
        stage.setMinHeight(450);
        stage.setMinWidth(650);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("editar", f);
        controller.setTela(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void excluir() throws DaoException, ControleException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            Funcionario f = (Funcionario) tbFuncionario.getSelectionModel().getSelectedItem();
            alert.setTitle("Excluir funcionario");
            alert.setHeaderText("Você tem certeza que excluir o(a) funcionario(a) " + f.getNmFuncionario() + "?");
            alert.setContentText("Se tiver certeza, clique em OK para confirmar a exclusão");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                s.excluir(f);
                listaFuncionario();
                desabilitarBotoes();
                atualizarTabela();
            } else {
                desabilitarBotoes();
            }
        } catch (ControleException | DaoException e) {
            desabilitarBotoes();
        }
    }

    @FXML
    private void visualizar() throws IOException, DaoException {
        Funcionario f = (Funcionario) tbFuncionario.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudFuncionariosUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudFuncionariosUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setMinHeight(400);
        stage.setMinWidth(600);
        stage.setResizable(false);
        stage.setTitle("Visualizar funcionario");
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("visualizar", f);
        controller.setTela(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        atualizarTabela();
    }

    //UTEIS
    @FXML
    private void habilitarBotoes(MouseEvent event) throws IOException, DaoException {
        if (tbFuncionario.getSelectionModel().getSelectedItem() != null) {
            btEditar.setDisable(false);
            btExcluir.setDisable(false);
            btVisualizar.setDisable(false);

            if (event.getClickCount() > 1) {
                visualizar();
            }
        }
    }

    private void desabilitarBotoes() {
        btEditar.setDisable(true);
        btExcluir.setDisable(true);
        btVisualizar.setDisable(true);
    }

    @FXML
    private void pesquisar(KeyEvent event) throws DaoException, ControleException {
        if (event.getCode() == KeyCode.ENTER) {
            if (!"".equals(tfPesquisa.getText())) {
                try {
                    int codigo = Integer.parseInt(tfPesquisa.getText());
                    Funcionario f = s.consultarFuncionarioPorCod(codigo);
                    List<Funcionario> fList = new ArrayList();
                    fList.add(f);
                    atualizarTabela(fList);
                } catch (DaoException | NumberFormatException e) {
                    List<Funcionario> fList = s.filtroFuncionario(tfPesquisa.getText());
                    atualizarTabela(fList);
                }
            } else {
                atualizarTabela(listaFuncionario());
            }
        }
    }

}
