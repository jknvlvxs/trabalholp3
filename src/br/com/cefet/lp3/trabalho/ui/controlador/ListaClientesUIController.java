/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.ui.Programa;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
public class ListaClientesUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();

    @FXML
    private JFXButton btEditar;
    @FXML
    private JFXButton btExcluir;
    @FXML
    private JFXButton btVisualizar;
    @FXML
    private JFXButton btRelatorio;
    @FXML
    private JFXTextField tfPesquisa;
    @FXML
    TableView tbCliente;
    @FXML
    TableColumn<Cliente, Integer> codigoCol;
    @FXML
    TableColumn<Cliente, String> nomeCol;
    @FXML
    TableColumn<Cliente, String> cpfCol;
    @FXML
    TableColumn<Cliente, String> telefoneCol;
    @FXML
    TableColumn<Cliente, String> usuarioCol;
    @FXML
    TableColumn<Cliente, String> senhaCol;
    @FXML
    JFXButton btClientes;
    @FXML
    JFXButton btFuncionarios;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        codigoCol.setCellValueFactory(new PropertyValueFactory<>("cdCliente"));
        nomeCol.setCellValueFactory(new PropertyValueFactory<>("nmCliente"));
        cpfCol.setCellValueFactory(new PropertyValueFactory<>("cpfCliente"));
        telefoneCol.setCellValueFactory(new PropertyValueFactory<>("telCliente"));
        usuarioCol.setCellValueFactory(new PropertyValueFactory<>("usuarioCliente"));
        senhaCol.setCellValueFactory(new PropertyValueFactory<>("senhaCliente"));

        atualizarTabela();
    }

    private void atualizarTabela(List<Cliente> clientes) {
        for (int i = 0; i < tbCliente.getItems().size(); i++) {
            tbCliente.getItems().clear();
        }
        for (Cliente c : clientes) {
            tbCliente.getItems().add(c);
        }
    }

    public void atualizarTabela() {
        try {
            tbCliente.setItems(listaCliente());
        } catch (DaoException ex) {
            System.out.println("Erro ao preencher a tabela");
        }
    }

    private ObservableList<Cliente> listaCliente() throws DaoException {
        ObservableList<Cliente> list;
        list = FXCollections.observableArrayList();
        for (int i = 0; i < s.consultarTodosClientes().size(); i++) {
            list.add(s.consultarTodosClientes().get(i));
        }
        return list;
    }

    @FXML
    private void vendas() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("vendas", "clientes", null);
    }

    @FXML
    private void produtos() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("produtos", "clientes", null);
    }

    @FXML
    private void funcionarios() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("funcionarios", "clientes", null);
    }

    @FXML
    private void voltar() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("menu", "clientes", null);
    }

    @FXML
    private void relatorio() {
        desabilitarBotoes();
        tfPesquisa.requestFocus();
        Programa.changeScene("rclientes", "clientes", null);
    }

    //CRUD
    @FXML
    private void cadastrar() throws IOException, DaoException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudClientesUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudClientesUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Cadastrar cliente");
        stage.setMinHeight(600);
        stage.setMinWidth(750);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("cadastrar", null);
        controller.setTela(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void editar() throws IOException, DaoException {
        Cliente c = (Cliente) tbCliente.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudClientesUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudClientesUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setTitle("Editar cliente");
        stage.setMinHeight(600);
        stage.setMinWidth(750);
        stage.setResizable(false);
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("editar", c);
        controller.setTela(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        atualizarTabela();
    }

    @FXML
    private void excluir() throws DaoException, ControleException {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            Cliente c = (Cliente) tbCliente.getSelectionModel().getSelectedItem();
            alert.setTitle("Excluir cliente");
            alert.setHeaderText("Você tem certeza que excluir o(a) cliente " + c.getNmCliente() + "?");
            alert.setContentText("Se tiver certeza, clique em OK para confirmar a exclusão");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                s.excluir(c);
                atualizarTabela();
                desabilitarBotoes();
            } else {
                desabilitarBotoes();
            }
        } catch (ControleException | DaoException e) {
            desabilitarBotoes();
        }
    }

    @FXML
    private void visualizar() throws IOException, DaoException {
        Cliente c = (Cliente) tbCliente.getSelectionModel().getSelectedItem();
        desabilitarBotoes();
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/br/com/cefet/lp3/trabalho/ui/fxml/CrudClientesUI.fxml"));
        Parent fxml = loader.load();
        Scene scene = new Scene(fxml);
        CrudClientesUIController controller = loader.getController();
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setMinHeight(600);
        stage.setMinWidth(750);
        stage.setResizable(false);
        stage.setTitle("Visualizar cliente");
        stage.getIcons().add(new Image(getClass().getResource("imagens/MercaDjin.png").toExternalForm()));
        controller.ajustarTela("visualizar", c);
        controller.setTela(stage);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
    }

    // UTEIS
    @FXML
    private void habilitarBotoes(MouseEvent event) throws IOException, DaoException {
        if (tbCliente.getSelectionModel().getSelectedItem() != null) {
            btEditar.setDisable(false);
            btExcluir.setDisable(false);
            btVisualizar.setDisable(false);

            if (event.getClickCount() > 1) {
                visualizar();
            }
        }
    }

    private void desabilitarBotoes() {
        btEditar.setDisable(true);
        btExcluir.setDisable(true);
        btVisualizar.setDisable(true);
    }

    @FXML
    private void pesquisar(KeyEvent event) throws DaoException, ControleException {
        if (event.getCode() == KeyCode.ENTER) {
            if (!"".equals(tfPesquisa.getText())) {
                try {
                    int codigo = Integer.parseInt(tfPesquisa.getText());
                    Cliente c = s.consultarClientePorCod(codigo);
                    List<Cliente> cList = new ArrayList();
                    cList.add(c);
                    atualizarTabela(cList);
                } catch (DaoException | NumberFormatException e) {
                    List<Cliente> cList = s.filtroCliente(tfPesquisa.getText());
                    atualizarTabela(cList);
                }
            } else {
                atualizarTabela(listaCliente());
            }
        }
    }
}
