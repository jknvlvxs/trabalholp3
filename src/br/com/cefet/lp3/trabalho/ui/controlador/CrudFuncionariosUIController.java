/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 *
 * @author ADMIN
 */
public class CrudFuncionariosUIController implements Initializable {

    Stage tela;
    Supermercado s = Supermercado.getInstance();
    private String origem;
    private Funcionario funcionario;

    @FXML
    private JFXTextField tfCodigo;
    @FXML
    private JFXTextField tfNome;
    @FXML
    private JFXTextField tfUsuario;
    @FXML
    private JFXPasswordField tfSenha;
    @FXML
    private JFXButton btCancelar;
    @FXML
    private JFXButton btConfirmar;
    @FXML
    private Label lbTitulo;

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btConfirmar.setDefaultButton(true);
    }

    public void ajustarTela(String origem, Funcionario f) throws DaoException {
        setOrigem(origem);
        switch (origem) {
            case "cadastrar":
                tfCodigo.setText(Integer.toString(s.getProximoCodigoFuncionario()));
                break;
            case "editar":
                lbTitulo.setText("EDIÇÃO DE FUNCIONARIO");
                setFuncionario(f);
                tfCodigo.setDisable(true);
                tfCodigo.setText(Integer.toString(f.getCdFuncionario()));
                tfNome.setText(f.getNmFuncionario());
                tfUsuario.setText(f.getUsuarioFuncionario());
                tfSenha.setText(f.getSenhaFuncionario());
                break;
            case "visualizar":
                lbTitulo.setText("VISUALIZAÇÃO DE FUNCIONARIO");
                btCancelar.setVisible(false);
                btConfirmar.setText("Fechar");
                setFuncionario(f);
                tfCodigo.setDisable(true);
                tfNome.setDisable(true);
                tfUsuario.setDisable(true);
                tfSenha.setDisable(true);
                tfCodigo.setText(Integer.toString(f.getCdFuncionario()));
                tfNome.setText(f.getNmFuncionario());
                tfUsuario.setText(f.getUsuarioFuncionario());
                tfSenha.setText(f.getSenhaFuncionario());
                break;
        }
    }

    @FXML
    private void cancelar() {
        this.getTela().close();
    }

    @FXML
    private void confirmar() throws ControleException, DaoException {
        Funcionario f;
        switch (getOrigem()) {
            case "cadastrar":
                if (validar()) {
                    f = new Funcionario();
                    f.setNmFuncionario(tfNome.getText());
                    f.setUsuarioFuncionario(tfUsuario.getText());
                    f.setSenhaFuncionario(tfSenha.getText());
                    s.inserir(f);
                    cancelar();
                }

                break;
            case "editar":
                if (validar()) {
                    f = getFuncionario();
                    f.setNmFuncionario(tfNome.getText());
                    f.setUsuarioFuncionario(tfUsuario.getText());
                    f.setSenhaFuncionario(tfSenha.getText());
                    s.alterar(f);
                    cancelar();
                }

                break;
            case "visualizar":
                cancelar();
                break;

        }
    }

    private boolean validar() {
        String vazios = null;
        if (tfNome.getText().length() <= 1 || tfNome.getText().length() > 45) {
            tfNome.requestFocus();
            vazios = tfNome.getPromptText();
        }
        if (tfUsuario.getText().length() <= 1 || tfUsuario.getText().length() > 20) {
            if (vazios == null) {
                tfUsuario.requestFocus();
                vazios += " " + tfUsuario.getPromptText();

            }

        }
        if (tfSenha.getText().length() <= 1 || tfSenha.getText().length() > 20) {
            if (vazios == null) {
                tfSenha.requestFocus();
                vazios += " " + tfSenha.getPromptText();
            }
        }
        return vazios == null;
    }
}
