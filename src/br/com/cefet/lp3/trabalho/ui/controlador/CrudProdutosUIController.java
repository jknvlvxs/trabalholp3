/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.ui.controlador;

import br.com.cefet.lp3.trabalho.controle.ControleException;
import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 *
 * @author ADMIN
 */
public class CrudProdutosUIController implements Initializable {

    Supermercado s = Supermercado.getInstance();
    Stage tela;
    String origem;
    Produto produto;
    @FXML
    private JFXButton btProcurar;
    @FXML
    private JFXButton btCancelar;
    @FXML
    private JFXButton btConfirmar;
    @FXML
    private JFXTextField tfCodigo;
    @FXML
    private JFXTextField tfNome;
    @FXML
    private JFXTextField tfPreco;
    @FXML
    private JFXTextField tfQuantidade;
    @FXML
    private JFXTextField tfCategoria;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public Stage getTela() {
        return tela;
    }

    public void setTela(Stage tela) {
        this.tela = tela;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btProcurar.setDisable(true);
        btConfirmar.setDefaultButton(true);
    }

    public void ajustarTela(String origem, Produto p) throws DaoException {
        setOrigem(origem);
        switch (origem) {
            case "cadastrar":
                tfCodigo.setText(Integer.toString(s.getProximoCodigoProduto()));
                break;
            case "editar":
                setProduto(p);
                tfCodigo.setText(Integer.toString(p.getCdProduto()));
                tfNome.setText(p.getNmProduto());
                tfPreco.setText(Double.toString(p.getPrecoProduto()));
                tfQuantidade.setText(Integer.toString(p.getQuantEstoque()));
                tfCategoria.setText(p.getCategoriaProduto());
                break;
            case "visualizar":
                tfNome.setDisable(true);
                tfPreco.setDisable(true);
                tfQuantidade.setDisable(true);
                tfCategoria.setDisable(true);
                tfCodigo.setText(Integer.toString(p.getCdProduto()));
                tfCategoria.setText(p.getCategoriaProduto());
                tfNome.setText(p.getNmProduto());
                tfPreco.setText(Double.toString(p.getPrecoProduto()));
                tfQuantidade.setText(Integer.toString(p.getQuantEstoque()));
                btCancelar.setVisible(false);
                btConfirmar.setText("Fechar");
                break;
        }
    }

    @FXML
    private void confirmar() throws DaoException, ControleException {
        Produto p;
        switch (origem) {
            case "cadastrar":
                if (validar()) {
                    p = new Produto();
                    p.setNmProduto(tfNome.getText());
                    p.setCategoriaProduto(tfCategoria.getText());
                    p.setPrecoProduto(Double.parseDouble(tfPreco.getText()));
                    p.setQuantEstoque(Integer.parseInt(tfQuantidade.getText()));
                    s.inserir(p);
                    cancelar();
                }
                break;
            case "editar":
                if (validar()) {
                    p = getProduto();
                    p.setNmProduto(tfNome.getText());
                    p.setCategoriaProduto(tfCategoria.getText());
                    p.setPrecoProduto(Double.parseDouble(tfPreco.getText()));
                    p.setQuantEstoque(Integer.parseInt(tfQuantidade.getText()));
                    s.alterar(p);
                    cancelar();
                }
                break;

            case "visualizar":
                this.getTela().close();
                break;
        }
    }

    @FXML
    private void cancelar() {
        this.getTela().close();
    }

    @FXML
    private void procurar() {
        this.getTela().close();
    }

    private boolean validar() {
        String vazios = null;
        if (tfNome.getText().length() <= 1 || tfNome.getText().length() > 45) {
            tfNome.requestFocus();
            vazios = tfNome.getPromptText();
        }

        try {
            Double.parseDouble(tfPreco.getText());
        } catch (NumberFormatException e) {
            if (vazios == null) {
                tfPreco.requestFocus();
                vazios += " " + tfPreco.getPromptText();
            }
        }

        if (tfCategoria.getText().length() <= 1 || tfCategoria.getText().length() > 30) {
            if (vazios == null) {
                tfCategoria.requestFocus();
                vazios += " " + tfCategoria.getPromptText();
            }
        }

        try {
            Integer.parseInt(tfQuantidade.getText());
        } catch (NumberFormatException e) {
            if (vazios == null) {
                tfQuantidade.requestFocus();
                vazios += " " + tfQuantidade.getPromptText();
            }
        }
        return vazios == null;
    }
}
