package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.Cliente;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteDao extends Dao {

    private final String SQL_INSERIR
            = " insert into tbCliente (nmCliente, cpfCliente, telCliente, usuarioCliente, senhaCliente) "
            + " values (?,?,?,?,?) ";

    private final String SQL_CONSULTAR_POR_COD
            = " select * from tbCliente "
            + " where cdCliente  = ? ";

    private final String SQL_CONSULTAR_TODOS
            = " select * "
            + " from tbCliente";

    private final String SQL_CONSULTAR_POR_NOME
            = " select * "
            + " FROM tbCliente "
            + " where nmCliente like ? ";

    private final String SQL_EXCLUIR
            = " delete from tbCliente "
            + " where cdCliente = ? ";

    private final String SQL_ALTERAR
            = " update tbCliente "
            + " set nmCliente = ?, "
            + " cpfCliente = ?, "
            + " telCliente = ?, "
            + " usuarioCliente = ?, "
            + " senhaCliente = ? "
            + " where cdCliente = ? ";

    private final String SQL_PROXCD
            = " select * "
            + " from information_schema.tables "
            + " where table_schema = DATABASE() "
            + " and table_name = 'tbcliente' ";

    private final String FILTRO = "SELECT * FROM tbcliente c "
            + " WHERE "
            + "(c.nmcliente like ? "
            + " OR c.usuariocliente like ?) ";

    public int proximoCodigo() throws DaoException {
        int cd = 0;
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_PROXCD);
            rs = ps.executeQuery();
            while (rs.next()) {
                cd = rs.getInt("AUTO_INCREMENT");
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método FuncionarioDao.proximoCodigo " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return cd;
    }

    public int inserir(Cliente c) throws DaoException {
        int ret = -1;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_INSERIR, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, c.getNmCliente());
            ps.setString(2, c.getCpfCliente());
            ps.setString(3, c.getTelCliente());
            ps.setString(4, c.getUsuarioCliente());
            ps.setString(5, c.getSenhaCliente());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ret = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DaoException("Erro no ClienteDao.inserir (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }

        return ret;
    }

    public Cliente consultarClientePorCod(int cod) throws DaoException {
        Cliente ret = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            if (rs.next()) {
                ret = getClienteFromRs(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no ClienteDao.consultarPorCod (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public List<Cliente> consultarClientePorNome(String nome) throws DaoException {
        List<Cliente> aList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_NOME);
            ps.setString(1, nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Cliente cli = getClienteFromRs(rs);
                aList.add(cli);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no ClienteDao.consultarPorNome (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return aList;
    }

    public List<Cliente> consultarTodosClientes() throws DaoException {

        List<Cliente> aList = new ArrayList();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_CONSULTAR_TODOS);

            rs = ps.executeQuery();

            while (rs.next()) {
                Cliente cli = getClienteFromRs(rs);
                aList.add(cli);
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método ClienteDao.consultarTodos " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return aList;
    }

    public void alterar(Cliente c) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_ALTERAR);

            ps.setString(1, c.getNmCliente());
            ps.setString(2, c.getCpfCliente());
            ps.setString(3, c.getTelCliente());
            ps.setString(4, c.getUsuarioCliente());
            ps.setString(5, c.getSenhaCliente());
            ps.setInt(6, c.getCdCliente());

            ps.execute();
        } catch (SQLException ex) {
            throw new DaoException("Erro no método AlunoDao.alterar (" + c.getNmCliente() + ") " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    public boolean excluir(Cliente c) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_EXCLUIR);
            ps.setInt(1, c.getCdCliente());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            throw new DaoException("Erro no método ClienteDao.excluir(" + c.getNmCliente() + ")-: " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    private Cliente getClienteFromRs(ResultSet rs) throws DaoException {
        Cliente c = new Cliente();
        try {
            c.setCdCliente(rs.getInt("cdCliente"));
            c.setNmCliente(rs.getString("nmCliente"));
            c.setCpfCliente(rs.getString("cpfCliente"));
            c.setTelCliente(rs.getString("telCliente"));
            c.setUsuarioCliente(rs.getString("usuarioCliente"));
            c.setSenhaCliente(rs.getString("senhaCliente"));
        } catch (SQLException e) {
            throw new DaoException("Erro no AlunoDao.getClienteFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return c;
    }

    public List<Cliente> filtro(String s) throws DaoException {
        List<Cliente> cList = new ArrayList();

        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(FILTRO);
            ps.setString(1, s + "%");
            ps.setString(2, s + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                boolean test = false;
                Cliente c = getClienteFromRs(rs);
                for (int i = 0; i < cList.size(); i++) {
                    if (c.getCdCliente() == cList.get(i).getCdCliente()) {
                        test = true;
                        break;
                    }
                }
                if (!test) {
                    cList.add(c);
                }
                test = false;
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no metodo Cliente.filtrar(): " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return cList;
    }

}
