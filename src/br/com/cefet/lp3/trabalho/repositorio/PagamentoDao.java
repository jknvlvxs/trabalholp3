package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.Pagamento;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PagamentoDao extends Dao {

    private final String SQL_INSERIR
            = " insert into tbPagamento (dtPagamento, situacaoPagamento, cdVenda) "
            + " values (?,?,?) ";

    private final String SQL_CONSULTAR_POR_COD
            = " select * from tbPagamento "
            + " where cdPagamento  = ? ";

    private final String SQL_CONSULTAR_POR_COD_VENDA
            = " select * from tbPagamento "
            + " where cdVenda  = ? ";

    private final String SQL_CONSULTAR_TODOS
            = " select * "
            + " from tbPagamento";

    private final String SQL_EXCLUIR
            = " delete from tbPagamento "
            + " where cdPagamento=? ";

    private final String SQL_ALTERAR
            = " update tbPagamento "
            + " set dtPagamento = ?, "
            + " situacaoPagamento = ?, "
            + " cdVenda = ?"
            + " where cdPagamento=? ";

    public int inserir(Pagamento f) throws DaoException {
        int ret = -1;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_INSERIR, Statement.RETURN_GENERATED_KEYS);
            Date data = new Date(f.getDtPagamento().getTime());
            ps.setDate(1, data);
            ps.setString(2, f.getSituacaoPagamento());
            ps.setInt(3, f.getCdVenda());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ret = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DaoException("Erro no PagamentoDao.inserir (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public Pagamento consultarPagamentoPorCod(int cod) throws DaoException {
        Pagamento ret = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            if (rs.next()) {
                ret = getPagamentoFromRs(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no PagamentoDao.consultarPorCod (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public List<Pagamento> consultarPagamentoPorCodVenda(int cod) throws DaoException {
        List<Pagamento> pgList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD_VENDA);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            while (rs.next()) {
                Pagamento pg = getPagamentoFromRs(rs);
                pgList.add(pg);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no PagamentoDao.consultarPorCodVenda (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return pgList;
    }

    public List<Pagamento> consultarTodosPagamento() throws DaoException {

        List<Pagamento> fList = new ArrayList();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_CONSULTAR_TODOS);
            rs = ps.executeQuery();

            while (rs.next()) {
                Pagamento fun = getPagamentoFromRs(rs);
                fList.add(fun);
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método PagamentoDao.consultarTodos " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return fList;
    }

    public void alterar(Pagamento f) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_ALTERAR);
            Date data = new Date(f.getDtPagamento().getTime());
            ps.setDate(1, data);
            ps.setString(2, f.getSituacaoPagamento());
            ps.setInt(3, f.getCdVenda());
            ps.setInt(4, f.getCdPagamento());
            ps.execute();
        } catch (SQLException ex) {
            throw new DaoException("Erro no método PagamentoDao.alterar (" + f.getCdPagamento() + ") " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    public boolean excluir(Pagamento f) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_EXCLUIR);
            ps.setInt(1, f.getCdPagamento());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            throw new DaoException("Erro no método PagamentoDao.excluir(" + f.getCdPagamento() + ")-: " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    private Pagamento getPagamentoFromRs(ResultSet rs) throws DaoException {
        Pagamento f = new Pagamento();
        try {
            f.setCdPagamento(rs.getInt("cdPagamento"));
            f.setDtPagamento(rs.getDate("dtPagamento"));
            f.setSituacaoPagamento(rs.getString("situacaoPagamento"));
            f.setCdVenda(rs.getInt("cdVenda"));
        } catch (SQLException e) {
            throw new DaoException("Erro no PagamentoDao.getPagamentoFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return f;
    }

}
