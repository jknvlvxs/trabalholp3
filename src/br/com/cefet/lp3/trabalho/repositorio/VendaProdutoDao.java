package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.VendaProduto;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VendaProdutoDao extends Dao {

    private final String SQL_INSERIR
            = " insert into tbVenda_Produto (cdVenda, cdProduto, qtdProdutoVenda, precoProdutoVenda) "
            + " values (?,?,?,?) ";

    private final String SQL_CONSULTAR_POR_COD
            = " select * from tbVenda_Produto "
            + " where cdVenda = ? "
            + " and cdProduto = ? ";

    private final String SQL_CONSULTAR_POR_COD_VENDA
            = " select * from tbVenda_Produto "
            + " where cdVenda = ? ";

    private final String SQL_CONSULTAR_POR_COD_PRODUTO
            = " select * from tbVenda_Produto "
            + " where cdProduto  = ? ";

    private final String SQL_CONSULTAR_TODOS
            = " select * "
            + " from tbVenda_Produto ";

    private final String SQL_EXCLUIR
            = " delete from tbVenda_Produto "
            + " where cdVenda = ? "
            + " and cdProduto = ? ";

    private final String SQL_ALTERAR
            = " update tbVenda_Produto "
            + " set qtdProdutoVenda = ?, "
            + " precoProdutoVenda = ? "
            + " where cdVenda = ?"
            + " and cdProduto = ?";

    public int inserir(VendaProduto vp) throws DaoException {
        int ret = -1;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_INSERIR, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, vp.getCdVenda());
            ps.setInt(2, vp.getCdProduto());
            ps.setInt(3, vp.getQtdProdutoVenda());
            ps.setDouble(4, vp.getPrecoProdutoVenda());

            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ret = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DaoException("Erro no VendaProdutoDao.inserir (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public VendaProduto consultarPorCod(int codVenda, int codProduto) throws DaoException {
        VendaProduto ret = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD);
            ps.setInt(1, codVenda);
            ps.setInt(2, codProduto);
            rs = ps.executeQuery();
            if (rs.next()) {
                ret = getProdutoFromRs(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no ProdutoDao.consultarPorCod (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public List<VendaProduto> consultarPorCodVenda(int cod) throws DaoException {
        List<VendaProduto> vpList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD_VENDA);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            while (rs.next()) {
                VendaProduto vp = getProdutoFromRs(rs);
                vpList.add(vp);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no VendaProdutoDao.consultarPorCodVenda (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return vpList;
    }

    public List<VendaProduto> consultarPorCodProduto(int cod) throws DaoException {
        List<VendaProduto> vpList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD_PRODUTO);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            while (rs.next()) {
                VendaProduto vp = getProdutoFromRs(rs);
                vpList.add(vp);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no VendaProdutoDao.consultarPorCodProduto (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return vpList;
    }

    public List<VendaProduto> consultarTodos() throws DaoException {

        List<VendaProduto> vpList = new ArrayList();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_CONSULTAR_TODOS);

            rs = ps.executeQuery();

            while (rs.next()) {
                VendaProduto vp = getProdutoFromRs(rs);
                vpList.add(vp);
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método VendaProdutoDao.consultarTodos " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return vpList;
    }

    public void alterar(VendaProduto vp) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_ALTERAR);

            ps.setInt(1, vp.getQtdProdutoVenda());
            ps.setDouble(2, vp.getPrecoProdutoVenda());
            ps.setInt(3, vp.getCdVenda());
            ps.setInt(4, vp.getCdProduto());

            ps.execute();
        } catch (SQLException ex) {
            throw new DaoException("Erro no método VendaProdutoDao.alterar v(" + vp.getCdVenda() + ") p(" + vp.getCdProduto() + ") " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    public boolean excluir(VendaProduto vp) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_EXCLUIR);
            ps.setInt(1, vp.getCdVenda());
            ps.setInt(2, vp.getCdProduto());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            throw new DaoException("Erro no método VendaProdutoDao.excluir v(" + vp.getCdVenda() + ") p(" + vp.getCdProduto() + ") -: " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    private VendaProduto getProdutoFromRs(ResultSet rs) throws DaoException {
        VendaProduto vp = new VendaProduto();
        try {
            vp.setCdVenda(rs.getInt("cdVenda"));
            vp.setCdProduto(rs.getInt("cdProduto"));
            vp.setQtdProdutoVenda(rs.getInt("qtdProdutoVenda"));
            vp.setPrecoProdutoVenda(rs.getDouble("precoProdutoVenda"));
        } catch (SQLException e) {
            throw new DaoException("Erro no VendaProdutoDao.getProdutoFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return vp;
    }

}
