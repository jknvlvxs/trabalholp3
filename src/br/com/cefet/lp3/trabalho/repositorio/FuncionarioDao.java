package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FuncionarioDao extends Dao {

    private final String SQL_INSERIR
            = " insert into tbFuncionario (nmFuncionario, usuarioFuncionario, senhaFuncionario) "
            + " values (?,?,?) ";

    private final String SQL_CONSULTAR_POR_COD
            = " select * from tbFuncionario "
            + " where cdFuncionario  = ? ";

    private final String SQL_CONSULTAR_TODOS
            = " select * "
            + " from tbFuncionario";

    private final String SQL_CONSULTAR_POR_NOME
            = " select * "
            + " FROM tbFuncionario "
            + " where nmFuncionario like ? ";

    private final String SQL_EXCLUIR
            = " delete from tbFuncionario "
            + " where cdFuncionario=? ";

    private final String SQL_ALTERAR
            = " update tbFuncionario "
            + " set nmFuncionario = ?, "
            + " usuarioFuncionario = ?, "
            + " senhaFuncionario = ?"
            + " where cdFuncionario = ? ";

    private final String SQL_PROXCD
            = " select * "
            + " from information_schema.tables "
            + " where table_schema = DATABASE() "
            + " and table_name = 'tbfuncionario' ";

    private final String FILTRO = "SELECT * FROM tbfuncionario f "
            + " WHERE "
            + "(f.nmfuncionario like ? "
            + " OR f.usuariofuncionario like ?) ";

    public int proximoCodigo() throws DaoException {
        int cd = 0;
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_PROXCD);
            rs = ps.executeQuery();

            while (rs.next()) {
                cd = rs.getInt("AUTO_INCREMENT");
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método FuncionarioDao.proximoCodigo " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return cd;
    }

    public int inserir(Funcionario f) throws DaoException {
        int ret = -1;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_INSERIR, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, f.getNmFuncionario());
            ps.setString(2, f.getUsuarioFuncionario());
            ps.setString(3, f.getSenhaFuncionario());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ret = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DaoException("Erro no FuncionarioDao.inserir (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public Funcionario consultarFuncionarioPorCod(int cod) throws DaoException {
        Funcionario ret = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            if (rs.next()) {
                ret = getFuncionarioFromRs(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no FuncionarioDao.consultarPorCod (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public List<Funcionario> consultarFuncionarioPorNome(String nome) throws DaoException {
        List<Funcionario> fList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_NOME);
            ps.setString(1, nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Funcionario func = getFuncionarioFromRs(rs);
                fList.add(func);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no FuncionarioDao.consultarPorNome (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return fList;
    }

    public List<Funcionario> consultarTodosFuncionario() throws DaoException {

        List<Funcionario> fList = new ArrayList();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;;

        try {
            ps = conn.prepareStatement(SQL_CONSULTAR_TODOS);

            rs = ps.executeQuery();

            while (rs.next()) {
                Funcionario fun = getFuncionarioFromRs(rs);
                fList.add(fun);
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método FuncionarioDao.consultarTodos " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return fList;
    }

    public void alterar(Funcionario f) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_ALTERAR);

            ps.setString(1, f.getNmFuncionario());
            ps.setString(2, f.getUsuarioFuncionario());
            ps.setString(3, f.getSenhaFuncionario());
            ps.setInt(4, f.getCdFuncionario());
            ps.execute();
        } catch (SQLException ex) {
            throw new DaoException("Erro no método FuncionarioDao.alterar (" + f.getNmFuncionario() + ") " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    public boolean excluir(Funcionario f) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_EXCLUIR);
            ps.setInt(1, f.getCdFuncionario());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            throw new DaoException("Erro no método FuncionarioDao.excluir(" + f.getNmFuncionario() + ")-: " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    private Funcionario getFuncionarioFromRs(ResultSet rs) throws DaoException {
        Funcionario f = new Funcionario();
        try {
            f.setCdFuncionario(rs.getInt("cdFuncionario"));
            f.setNmFuncionario(rs.getString("nmFuncionario"));
            f.setUsuarioFuncionario(rs.getString("usuarioFuncionario"));
            f.setSenhaFuncionario(rs.getString("senhaFuncionario"));
        } catch (SQLException e) {
            throw new DaoException("Erro no FuncionarioDao.getFuncionarioFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return f;
    }

    public List<Funcionario> filtro(String s) throws DaoException {
        List<Funcionario> fList = new ArrayList();

        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(FILTRO);
            ps.setString(1, s + "%");
            ps.setString(2, s + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                boolean test = false;
                Funcionario f = getFuncionarioFromRs(rs);
                for (int i = 0; i < fList.size(); i++) {
                    if (f.getCdFuncionario() == fList.get(i).getCdFuncionario()) {
                        test = true;
                        break;
                    }
                }
                if (!test) {
                    fList.add(f);
                }
                test = false;
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no metodo Funcionario.filtrar(): " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return fList;
    }

}
