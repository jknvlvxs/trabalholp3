package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.ClienteEndereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClienteEnderecoDao extends Dao {

    private final String RELATORIO
            = " select c.cdCliente, nmCliente, cpfCliente, logradouroEnd, numeroEnd, bairroEnd, municipioEnd, ufEnd "
            + " from tbCliente c inner join tbEndereco e "
            + " on c.cdCliente = e.cdCliente"
            + " where bairroEnd like ? "
            + " and municipioEnd like ? "
            + " and nmCliente like ? "
            + " order by nmCliente; ";

    private ClienteEndereco getClienteEnderecoFromRs(ResultSet rs) throws DaoException {
        ClienteEndereco ce = new ClienteEndereco();
        try {
            ce.setCdCliente(rs.getInt("cdCliente"));
            ce.setNmCliente(rs.getString("nmCliente"));
            ce.setCpfCliente(rs.getString("cpfCliente"));
            ce.setLogradouroEnd(rs.getString("logradouroEnd"));
            ce.setNumeroEnd(rs.getString("numeroEnd"));
            ce.setBairroEnd(rs.getString("bairroEnd"));
            ce.setMunicipioEnd(rs.getString("municipioEnd"));
            ce.setUfEnd(rs.getString("ufEnd"));
        } catch (SQLException e) {
            throw new DaoException("Erro no ClienteEnderecoDao.getClienteEnderecoFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return ce;
    }

    public List<ClienteEndereco> relatorio(String bairro, String cidade, String cliente) throws DaoException {
        List<ClienteEndereco> ceList = new ArrayList();

        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(RELATORIO);
            ps.setString(1, bairro + "%");
            ps.setString(2, cidade + "%");
            ps.setString(3, cliente + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                boolean test = false;
                ClienteEndereco ce = getClienteEnderecoFromRs(rs);
                //for (int i = 0; i < ceList.size(); i++) {
                ceList.add(ce);
                //}
            }

        } catch (SQLException ex) {
            throw new DaoException("Erro no metodo Produto.relatorio(): " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return ceList;
    }
}
