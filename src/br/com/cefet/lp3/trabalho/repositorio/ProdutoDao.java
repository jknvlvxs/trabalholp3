package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.Produto;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProdutoDao extends Dao {

    private final String SQL_INSERIR
            = " insert into tbProduto (nmProduto, precoProduto, categoriaProduto, quantEstoque, imgProduto) "
            + " values (?,?,?,?,?) ";

    private final String SQL_CONSULTAR_POR_COD
            = " select * from tbProduto "
            + " where cdProduto  = ? ";

    private final String SQL_CONSULTAR_TODOS
            = " select * "
            + " from tbProduto ";

    private final String SQL_CONSULTAR_POR_NOME
            = " select * "
            + " FROM tbProduto "
            + " where nmProduto like ? ";

    private final String SQL_CONSULTAR_POR_CATEGORIA
            = " select * "
            + " FROM tbProduto "
            + " where categoriaProduto like ? ";

    private final String SQL_EXCLUIR
            = " delete from tbProduto "
            + " where cdProduto=? ";

    private final String SQL_ALTERAR
            = " update tbProduto "
            + " set nmProduto = ?, "
            + " precoProduto = ?, "
            + " categoriaProduto = ?, "
            + " quantEstoque = ?, "
            + " imgProduto = ? "
            + " where cdProduto = ? ";
    private final String SQL_PROXCD
            = " select * "
            + " from information_schema.tables "
            + " where table_schema = DATABASE() "
            + " and table_name = 'tbproduto' ";

    private final String FILTRO = "SELECT * FROM tbproduto p "
            + " WHERE "
            + "(p.nmproduto like ? "
            + " OR p.categoriaproduto like ?) ";

    private final String RELATORIO
            = " select cdProduto, nmProduto, precoProduto, categoriaProduto, quantEstoque "
            + " from tbProduto "
            + " where precoProduto > ? "
            + " and precoProduto <  ? "
            + " and categoriaProduto like ? ";

    public int proximoCodigo() throws DaoException {
        int cd = 0;
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_PROXCD);
            rs = ps.executeQuery();

            while (rs.next()) {
                cd = rs.getInt("AUTO_INCREMENT");
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método ProdutoDao.proximoCodigo " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return cd;
    }

    public int inserir(Produto prod) throws DaoException {
        int ret = -1;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_INSERIR, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, prod.getNmProduto());
            ps.setDouble(2, prod.getPrecoProduto());
            ps.setString(3, prod.getCategoriaProduto());
            ps.setInt(4, prod.getQuantEstoque());
            ps.setBlob(5, prod.getImgProduto());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ret = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DaoException("Erro no ProdutoDao.inserir (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public Produto consultarPorCod(int cod) throws DaoException {
        Produto ret = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            if (rs.next()) {
                ret = getProdutoFromRs(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no ProdutoDao.consultarPorCod (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public List<Produto> consultarPorNome(String nome) throws DaoException {
        List<Produto> pList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_NOME);
            ps.setString(1, nome + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Produto prod = getProdutoFromRs(rs);
                pList.add(prod);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no ProdutoDao.consultarPorNome (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return pList;
    }

    public List<Produto> consultarPorCategoria(String categoria) throws DaoException {
        List<Produto> pList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_CATEGORIA);
            ps.setString(1, categoria + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                Produto prod = getProdutoFromRs(rs);
                pList.add(prod);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no ProdutoDao.consultarPorCategoria (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return pList;
    }

    public List<Produto> consultarTodos() throws DaoException {

        List<Produto> pList = new ArrayList();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_CONSULTAR_TODOS);

            rs = ps.executeQuery();

            while (rs.next()) {
                Produto prod = getProdutoFromRs(rs);
                pList.add(prod);
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método ProdutoDao.consultarTodos " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return pList;
    }

    public void alterar(Produto p) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_ALTERAR);

            ps.setString(1, p.getNmProduto());
            ps.setDouble(2, p.getPrecoProduto());
            ps.setString(3, p.getCategoriaProduto());
            ps.setInt(4, p.getQuantEstoque());
            ps.setBlob(5, p.getImgProduto());
            ps.setInt(6, p.getCdProduto());

            ps.execute();
        } catch (SQLException ex) {
            throw new DaoException("Erro no método ProdutoDao.alterar (" + p.getNmProduto() + ") " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    public boolean excluir(Produto p) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_EXCLUIR);
            ps.setInt(1, p.getCdProduto());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            throw new DaoException("Erro no método ProdutoDao.excluir(" + p.getNmProduto() + ")-: " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    private Produto getProdutoFromRs(ResultSet rs) throws DaoException {
        Produto p = new Produto();
        try {
            p.setCdProduto(rs.getInt("cdProduto"));
            p.setNmProduto(rs.getString("nmProduto"));
            p.setPrecoProduto(rs.getDouble("precoProduto"));
            p.setCategoriaProduto(rs.getString("categoriaProduto"));
            p.setQuantEstoque(rs.getInt("quantEstoque"));

        } catch (SQLException e) {
            throw new DaoException("Erro no ProdutoDao.getProdutoFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return p;
    }

    public List<Produto> filtro(String s) throws DaoException {
        List<Produto> pList = new ArrayList();

        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(FILTRO);
            ps.setString(1, s + "%");
            ps.setString(2, s + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                boolean test = false;
                Produto p = getProdutoFromRs(rs);
                for (int i = 0; i < pList.size(); i++) {
                    if (p.getCdProduto() == pList.get(i).getCdProduto()) {
                        test = true;
                        break;
                    }
                }
                if (!test) {
                    pList.add(p);
                }
                test = false;
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no metodo Produto.filtrar(): " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return pList;
    }

    public List<Produto> relatorio(Double precoMin, Double precoMax, String categoria) throws DaoException {
        List<Produto> pList = new ArrayList();

        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(RELATORIO);
            ps.setDouble(1, precoMin);
            ps.setDouble(2, precoMax);
            ps.setString(3, categoria + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                boolean test = false;
                Produto p = getProdutoFromRs(rs);
                for (int i = 0; i < pList.size(); i++) {
                    if (p.getCdProduto() == pList.get(i).getCdProduto()) {
                        test = true;
                        break;
                    }
                }
                if (!test) {
                    pList.add(p);
                }
                test = false;
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no metodo Produto.relatorio(): " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return pList;
    }
}
