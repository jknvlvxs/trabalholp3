package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.Venda;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

public class VendaDao extends Dao {

    private final String SQL_INSERIR
            = " insert into tbVenda (dtVenda, formaPagamento, qtdParcelas, statusEntrega, precoFrete, precoCompra, precoTotal, cdCliente, cdFuncionario) "
            + " values (?,?,?,?,?,?,?,?,?) ";

    private final String SQL_CONSULTAR_POR_COD
            = " select * from tbVenda "
            + " where cdVenda  = ? ";

    private final String SQL_CONSULTAR_POR_COD_CLIENTE
            = " select * from tbVenda "
            + " where cdCliente  = ? ";

    private final String SQL_CONSULTAR_POR_COD_FUNC
            = " select * from tbVenda "
            + " where cdFuncionario  = ? ";

    private final String SQL_CONSULTAR_TODOS
            = " select * "
            + " from tbVenda ";

    private final String SQL_EXCLUIR
            = " delete from tbVenda "
            + " where cdVenda=? ";

    private final String SQL_ALTERAR
            = " update tbVenda "
            + " set dtVenda = ?, "
            + " formaPagamento = ?, "
            + " qtdParcelas = ?, "
            + " statusEntrega = ?, "
            + " precoFrete = ?, "
            + " precoCompra = ?, "
            + " precoTotal = ? "
            + " where cdVenda = ? ";

    private final String SQL_PROXCD
            = " select * "
            + " from information_schema.tables "
            + " where table_schema = DATABASE() "
            + " and table_name = 'tbvenda' ";

    private final String RELATORIO
            = " select * "
            + " from tbVenda "
            + " where precoTotal > ? "
            + " and precoTotal < ? "
            + " and statusEntrega like ? ";

    public int proximoCodigo() throws DaoException {
        int cd = 0;
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_PROXCD);
            rs = ps.executeQuery();

            while (rs.next()) {
                cd = rs.getInt("AUTO_INCREMENT");
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método VendaDao.proximoCodigo " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return cd;
    }

    public int inserir(Venda venda) throws DaoException {
        int ret = -1;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_INSERIR, Statement.RETURN_GENERATED_KEYS);
            Date data = new Date(venda.getDtVenda().getTime());
            ps.setDate(1, data);
            ps.setString(2, venda.getFormaPagamento());
            ps.setInt(3, venda.getQtdParcelas());
            ps.setString(4, venda.getStatusEntrega());
            ps.setDouble(5, venda.getPrecoFrete());
            ps.setDouble(6, venda.getPrecoCompra());
            ps.setDouble(7, venda.getPrecoTotal());
            if (venda.getCdCliente() == -1) {
                ps.setNull(8, Types.INTEGER);
            } else {
                ps.setInt(8, venda.getCdCliente());
            }
            if (venda.getCdFuncionario() == -1) {
                ps.setNull(9, Types.INTEGER);
            } else {
                ps.setInt(9, venda.getCdFuncionario());
            }
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ret = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DaoException("Erro no VendaDao.inserir (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public Venda consultarVendaPorCod(int cod) throws DaoException {
        Venda ret = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            if (rs.next()) {
                ret = getVendaFromRs(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no VendaDao.consultarPorCod (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public List<Venda> consultarVendaPorCodCliente(int cd) throws DaoException {
        List<Venda> vList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD_CLIENTE);
            ps.setInt(1, cd);
            rs = ps.executeQuery();
            while (rs.next()) {
                Venda venda = getVendaFromRs(rs);
                vList.add(venda);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no VendaDao.consultarPorCodCliente (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return vList;
    }

    public List<Venda> consultarPorCodFuncionario(int cd) throws DaoException {
        List<Venda> vList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD_FUNC);
            ps.setInt(1, cd);
            rs = ps.executeQuery();
            while (rs.next()) {
                Venda venda = getVendaFromRs(rs);
                vList.add(venda);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no VendaDao.consultarPorCodFuncionario (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return vList;
    }

    public List<Venda> consultarTodosVenda() throws DaoException {

        List<Venda> pList = new ArrayList();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_CONSULTAR_TODOS);

            rs = ps.executeQuery();

            while (rs.next()) {
                Venda prod = getVendaFromRs(rs);
                pList.add(prod);
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método VendaDao.consultarTodos " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return pList;
    }

    public void alterar(Venda venda) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_ALTERAR);

            Date data = new Date(venda.getDtVenda().getTime());
            ps.setDate(1, data);
            ps.setString(2, venda.getFormaPagamento());
            ps.setInt(3, venda.getQtdParcelas());
            ps.setString(4, venda.getStatusEntrega());
            ps.setDouble(5, venda.getPrecoFrete());
            ps.setDouble(6, venda.getPrecoCompra());
            ps.setDouble(7, venda.getPrecoTotal());
            ps.setInt(8, venda.getCdVenda());

            ps.execute();
        } catch (SQLException ex) {
            throw new DaoException("Erro no método VendaDao.alterar (" + venda.getCdVenda() + ") " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    public boolean excluir(Venda p) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_EXCLUIR);
            ps.setInt(1, p.getCdVenda());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            throw new DaoException("Erro no método VendaDao.excluir(" + p.getCdVenda() + ")-: " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    private Venda getVendaFromRs(ResultSet rs) throws DaoException {
        Venda v = new Venda();
        try {
            v.setCdVenda(rs.getInt("cdVenda"));
            v.setDtVenda(rs.getDate("dtVenda"));
            v.setFormaPagamento(rs.getString("formaPagamento"));
            v.setQtdParcelas(rs.getInt("qtdParcelas"));
            v.setStatusEntrega(rs.getString("statusEntrega"));
            v.setPrecoFrete(rs.getDouble("precoFrete"));
            v.setPrecoCompra(rs.getDouble("precoCompra"));
            v.setPrecoTotal(rs.getDouble("precoTotal"));
            v.setCdCliente(rs.getInt("cdCliente"));
            v.setCdFuncionario(rs.getInt("cdFuncionario"));

        } catch (SQLException e) {
            throw new DaoException("Erro no VendaDao.getVendaFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return v;
    }

    public List<Venda> relatorio(Double precoMin, Double precoMax, String status) throws DaoException {
        List<Venda> vList = new ArrayList();

        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(RELATORIO);
            ps.setDouble(1, precoMin);
            ps.setDouble(2, precoMax);
            ps.setString(3, status + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                boolean test = false;
                Venda v = getVendaFromRs(rs);
                for (int i = 0; i < vList.size(); i++) {
                    if (v.getCdVenda() == vList.get(i).getCdVenda()) {
                        test = true;
                        break;
                    }
                }
                if (!test) {
                    vList.add(v);
                }
                test = false;
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no metodo Venda.relatorio(): " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return vList;
    }
}
