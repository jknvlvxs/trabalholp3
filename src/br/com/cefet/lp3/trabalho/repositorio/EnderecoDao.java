package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.Endereco;
import com.mysql.jdbc.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EnderecoDao extends Dao {

    private final String SQL_INSERIR
            = " insert into tbEndereco (logradouroEnd, numeroEnd, bairroEnd, municipioEnd, ufEnd, cepEnd, cdCliente) "
            + " values (?,?,?,?,?,?,?) ";

    private final String SQL_CONSULTAR_POR_CD_CLIENTE
            = " select * "
            + " from  tbEndereco e inner join TbCliente c"
            + " on c.cdCliente = e.cdCliente"
            + " where e.cdCliente = ? ";

    private final String SQL_CONSULTAR_POR_COD_ENDERECO
            = " select * from tbEndereco "
            + " where cdEndereco  = ? ";

    private final String SQL_CONSULTAR_TODOS
            = " select * "
            + " from tbEndereco";

    private final String SQL_ALTERAR
            = " update tbEndereco "
            + " set logradouroEnd = ?, "
            + " numeroEnd = ?, "
            + " bairroEnd = ?, "
            + " municipioEnd = ?, "
            + " ufEnd = ?, "
            + " cepEnd = ?,"
            + " cdCliente = ? "
            + " where cdEndereco = ? ";

    private final String SQL_EXCLUIR
            = " delete from tbEndereco "
            + " where cdEndereco = ? ";

    private final String SQL_PROXCD
            = " select * "
            + " from information_schema.tables "
            + " where table_schema = DATABASE() "
            + " and table_name = 'tbendereco' ";

    public int proximoCodigo() throws DaoException {
        int cd = 0;
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_PROXCD);
            rs = ps.executeQuery();

            while (rs.next()) {
                cd = rs.getInt("AUTO_INCREMENT");
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método EnderecoDao.proximoCodigo " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return cd;
    }

    public int inserir(Endereco end) throws DaoException {
        int ret = -1;

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_INSERIR, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, end.getLogradouroEnd());
            ps.setString(2, end.getNumeroEnd());
            ps.setString(3, end.getBairroEnd());
            ps.setString(4, end.getMunicipioEnd());
            ps.setString(5, end.getUfEnd());
            ps.setString(6, end.getCepEnd());
            ps.setInt(7, end.getCdCliente());
            ps.execute();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                ret = rs.getInt(1);
            }

        } catch (SQLException e) {
            throw new DaoException("Erro no EnderecoDao.inserir (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public Endereco consultarEnderecoPorCod(int cod) throws DaoException {
        Endereco ret = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_COD_ENDERECO);
            ps.setInt(1, cod);
            rs = ps.executeQuery();
            if (rs.next()) {
                ret = getEnderecoFromRs(rs);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no EnderecoDao.consultarPorCodEndereco (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return ret;
    }

    public List<Endereco> consultarEnderecoPorCodCliente(int cd) throws DaoException {
        List<Endereco> eList = new ArrayList();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = getConnection();
            ps = conn.prepareStatement(SQL_CONSULTAR_POR_CD_CLIENTE);
            ps.setInt(1, cd);
            rs = ps.executeQuery();
            while (rs.next()) {
                Endereco end = getEnderecoFromRs(rs);
                eList.add(end);
            }
        } catch (SQLException e) {
            throw new DaoException("Erro no EnderecoDao.consultarPorCodCliente (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        } finally {
            close(conn, ps, rs);
        }
        return eList;
    }

    public List<Endereco> consultarTodos() throws DaoException {

        List<Endereco> eList = new ArrayList();
        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(SQL_CONSULTAR_TODOS);

            rs = ps.executeQuery();

            while (rs.next()) {
                Endereco end = getEnderecoFromRs(rs);
                eList.add(end);
            }
        } catch (SQLException ex) {
            throw new DaoException("Erro no método EnderecoDao.consultarTodos " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }

        return eList;
    }

    public void alterar(Endereco end) throws DaoException {

        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_ALTERAR);

            ps.setString(1, end.getLogradouroEnd());
            ps.setString(2, end.getNumeroEnd());
            ps.setString(3, end.getBairroEnd());
            ps.setString(4, end.getMunicipioEnd());
            ps.setString(5, end.getUfEnd());
            ps.setString(6, end.getCepEnd());
            ps.setInt(7, end.getCdCliente());
            ps.setInt(8, end.getCdEndereco());

            ps.execute();
        } catch (SQLException ex) {
            throw new DaoException("Erro no método EnderecoDao.alterar (" + end.getLogradouroEnd() + ") " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    public boolean excluir(Endereco end) throws DaoException {
        Connection conn = getConnection();
        PreparedStatement ps = null;

        try {
            ps = conn.prepareStatement(SQL_EXCLUIR);
            ps.setInt(1, end.getCdEndereco());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            throw new DaoException("Erro no método EnderecoDao.excluir(" + end.getLogradouroEnd() + ")-: " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps);
        }
    }

    private Endereco getEnderecoFromRs(ResultSet rs) throws DaoException {
        Endereco end = new Endereco();
        try {
            end.setCdEndereco(rs.getInt("cdEndereco"));
            end.setLogradouroEnd(rs.getString("logradouroEnd"));
            end.setNumeroEnd(rs.getString("numeroEnd"));
            end.setBairroEnd(rs.getString("bairroEnd"));
            end.setMunicipioEnd(rs.getString("municipioEnd"));
            end.setUfEnd(rs.getString("ufEnd"));
            end.setCepEnd(rs.getString("cepEnd"));
            end.setCdCliente(rs.getInt("cdCliente"));

        } catch (SQLException e) {
            throw new DaoException("Erro no EnderecoDao.getEnderecoFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return end;
    }

}
