package br.com.cefet.lp3.trabalho.repositorio;

import br.com.cefet.lp3.trabalho.entidade.ProdutoVenda;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProdutoVendaDao extends Dao {

    private final String RELATORIO
            = " select v.cdVenda, dtVenda, precoProdutoVenda,qtdProdutoVenda , nmProduto, categoriaProduto, precoProduto "
            + " from tbVenda v inner join tbvenda_produto vp"
            + " on v.cdvenda = vp.cdvenda"
            + " inner join tbproduto p"
            + " on vp.cdproduto = p.cdproduto "
            + " where qtdProdutoVenda > ? "
            + " and qtdProdutoVenda < ? "
            + " and nmProduto like ?"
            + " order by v.cdVenda; ";

    private ProdutoVenda getProdutoVendaFromRs(ResultSet rs) throws DaoException {
        ProdutoVenda ce = new ProdutoVenda();
        try {
            ce.setCdVenda(rs.getInt("cdVenda"));
            ce.setDtVenda(rs.getDate("dtVenda"));
            ce.setQtdProdutoVenda(rs.getInt("qtdProdutoVenda"));
            ce.setPrecoProdutoVenda(rs.getDouble("precoProdutoVenda"));
            ce.setNmProduto(rs.getString("nmProduto"));
            ce.setPrecoProduto(rs.getDouble("precoProduto"));
            ce.setCategoriaProduto(rs.getString("categoriaProduto"));

        } catch (SQLException e) {
            throw new DaoException("Erro no ProdutoVendaDao.getProdutoVendaFromRs (" + e.getClass().getName() + " )-: " + e.getMessage(), e);
        }
        return ce;
    }

    public List<ProdutoVenda> relatorio(int qtdMin, int qtdMax, String produto) throws DaoException {
        List<ProdutoVenda> ceList = new ArrayList();

        Connection conn = getConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = conn.prepareStatement(RELATORIO);
            ps.setInt(1, qtdMin);
            ps.setInt(2, qtdMax);
            ps.setString(3, produto + "%");

            rs = ps.executeQuery();

            while (rs.next()) {
                boolean test = false;
                ProdutoVenda ce = getProdutoVendaFromRs(rs);
                //for (int i = 0; i < ceList.size(); i++) {
                ceList.add(ce);
                //}
            }

        } catch (SQLException ex) {
            throw new DaoException("Erro no metodo ProdutoVenda.relatorio(): " + ex.getClass().getName() + " - " + ex.getMessage(), ex);
        } finally {
            close(conn, ps, rs);
        }
        return ceList;
    }
}
