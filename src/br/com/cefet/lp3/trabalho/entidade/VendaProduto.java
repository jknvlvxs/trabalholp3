package br.com.cefet.lp3.trabalho.entidade;

public class VendaProduto {

    private int cdVenda;
    private int cdProduto;
    private int qtdProdutoVenda;
    private double precoProdutoVenda;

    public int getCdVenda() {
        return cdVenda;
    }

    public void setCdVenda(int cdVenda) {
        this.cdVenda = cdVenda;
    }

    public int getCdProduto() {
        return cdProduto;
    }

    public void setCdProduto(int cdProduto) {
        this.cdProduto = cdProduto;
    }

    public int getQtdProdutoVenda() {
        return qtdProdutoVenda;
    }

    public void setQtdProdutoVenda(int qtdProdutoVenda) {
        this.qtdProdutoVenda = qtdProdutoVenda;
    }

    public double getPrecoProdutoVenda() {
        return precoProdutoVenda;
    }

    public void setPrecoProdutoVenda(double precoProdutoVenda) {
        this.precoProdutoVenda = precoProdutoVenda;
    }

}
