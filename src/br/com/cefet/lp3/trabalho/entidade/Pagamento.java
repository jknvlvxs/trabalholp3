package br.com.cefet.lp3.trabalho.entidade;

import java.util.Date;

public class Pagamento {

    private int cdPagamento;
    private Date dtPagamento;
    private String situacaoPagamento;
    private int cdVenda;

    public int getCdPagamento() {
        return cdPagamento;
    }

    public void setCdPagamento(int cdPagamento) {
        this.cdPagamento = cdPagamento;
    }

    public Date getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Date dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    public String getSituacaoPagamento() {
        return situacaoPagamento;
    }

    public void setSituacaoPagamento(String situacaoPagamento) {
        this.situacaoPagamento = situacaoPagamento;
    }

    public int getCdVenda() {
        return cdVenda;
    }

    public void setCdVenda(int cdVenda) {
        this.cdVenda = cdVenda;
    }

}
