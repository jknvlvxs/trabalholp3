/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.entidade;

import java.util.Date;

/**
 *
 * @author Júlio
 */
public class ProdutoVenda {

    private int cdVenda;
    private Date dtVenda;
    private int qtdProdutoVenda;
    private double precoProdutoVenda;
    private String nmProduto;
    private double precoProduto;
    private String categoriaProduto;

    public ProdutoVenda() {
    }

    public ProdutoVenda(int cdVenda, Date dtVenda, int qtdProdutoVenda, double precoProdutoVenda, String nmProduto, double precoProduto, String categoriaProduto) {
        this.cdVenda = cdVenda;
        this.dtVenda = dtVenda;
        this.qtdProdutoVenda = qtdProdutoVenda;
        this.precoProdutoVenda = precoProdutoVenda;
        this.nmProduto = nmProduto;
        this.precoProduto = precoProduto;
        this.categoriaProduto = categoriaProduto;
    }

    public int getCdVenda() {
        return cdVenda;
    }

    public void setCdVenda(int cdVenda) {
        this.cdVenda = cdVenda;
    }

    public Date getDtVenda() {
        return dtVenda;
    }

    public void setDtVenda(Date dtVenda) {
        this.dtVenda = dtVenda;
    }

    public int getQtdProdutoVenda() {
        return qtdProdutoVenda;
    }

    public void setQtdProdutoVenda(int qtdProdutoVenda) {
        this.qtdProdutoVenda = qtdProdutoVenda;
    }

    public double getPrecoProdutoVenda() {
        return precoProdutoVenda;
    }

    public void setPrecoProdutoVenda(double precoProdutoVenda) {
        this.precoProdutoVenda = precoProdutoVenda;
    }

    public String getNmProduto() {
        return nmProduto;
    }

    public void setNmProduto(String nmProduto) {
        this.nmProduto = nmProduto;
    }

    public double getPrecoProduto() {
        return precoProduto;
    }

    public void setPrecoProduto(double precoProduto) {
        this.precoProduto = precoProduto;
    }

    public String getCategoriaProduto() {
        return categoriaProduto;
    }

    public void setCategoriaProduto(String categoriaProduto) {
        this.categoriaProduto = categoriaProduto;
    }

}
