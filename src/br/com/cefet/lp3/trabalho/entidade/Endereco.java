package br.com.cefet.lp3.trabalho.entidade;

public class Endereco {

    private int cdEndereco;
    private String logradouroEnd;
    private String numeroEnd;
    private String bairroEnd;
    private String municipioEnd;
    private String ufEnd;
    private String cepEnd;
    private int cdCliente;

    public int getCdEndereco() {
        return cdEndereco;
    }

    public void setCdEndereco(int cdEndereco) {
        this.cdEndereco = cdEndereco;
    }

    public String getLogradouroEnd() {
        return logradouroEnd;
    }

    public void setLogradouroEnd(String logradouroEnd) {
        this.logradouroEnd = logradouroEnd;
    }

    public String getNumeroEnd() {
        return numeroEnd;
    }

    public void setNumeroEnd(String numeroEnd) {
        this.numeroEnd = numeroEnd;
    }

    public String getBairroEnd() {
        return bairroEnd;
    }

    public void setBairroEnd(String bairroEnd) {
        this.bairroEnd = bairroEnd;
    }

    public String getMunicipioEnd() {
        return municipioEnd;
    }

    public void setMunicipioEnd(String municipioEnd) {
        this.municipioEnd = municipioEnd;
    }

    public String getUfEnd() {
        return ufEnd;
    }

    public void setUfEnd(String ufEnd) {
        this.ufEnd = ufEnd;
    }

    public String getCepEnd() {
        return cepEnd;
    }

    public void setCepEnd(String cepEnd) {
        this.cepEnd = cepEnd;
    }

    public int getCdCliente() {
        return cdCliente;
    }

    public void setCdCliente(int cdCliente) {
        this.cdCliente = cdCliente;
    }

}
