package br.com.cefet.lp3.trabalho.entidade;

public class Cliente {

    private int cdCliente;
    private String nmCliente;
    private String cpfCliente;
    private String telCliente;
    private String usuarioCliente;
    private String senhaCliente;

    public Cliente(int cdCliente, String nmCliente, String cpfCliente, String telCliente, String usuarioCliente, String senhaCliente) {
        this.cdCliente = cdCliente;
        this.nmCliente = nmCliente;
        this.cpfCliente = cpfCliente;
        this.telCliente = telCliente;
        this.usuarioCliente = usuarioCliente;
        this.senhaCliente = senhaCliente;
    }

    public Cliente() {

    }

    public int getCdCliente() {
        return cdCliente;
    }

    public void setCdCliente(int cdCliente) {
        this.cdCliente = cdCliente;
    }

    public String getNmCliente() {
        return nmCliente;
    }

    public void setNmCliente(String nmCliente) {
        this.nmCliente = nmCliente;
    }

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public String getTelCliente() {
        return telCliente;
    }

    public void setTelCliente(String telCliente) {
        this.telCliente = telCliente;
    }

    public String getUsuarioCliente() {
        return usuarioCliente;
    }

    public void setUsuarioCliente(String usuarioCliente) {
        this.usuarioCliente = usuarioCliente;
    }

    public String getSenhaCliente() {
        return senhaCliente;
    }

    public void setSenhaCliente(String senhaCliente) {
        this.senhaCliente = senhaCliente;
    }

}
