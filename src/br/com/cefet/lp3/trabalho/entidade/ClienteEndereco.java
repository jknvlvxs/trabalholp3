/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.entidade;

/**
 *
 * @author Júlio
 */
public class ClienteEndereco {

    private int cdCliente;
    private String nmCliente;
    private String cpfCliente;
    private String logradouroEnd;
    private String numeroEnd;
    private String bairroEnd;
    private String municipioEnd;
    private String ufEnd;

    public ClienteEndereco(int cdCliente, String nmCliente, String cpfCliente, String logradouroEnd, String numeroEnd, String bairroEnd, String municipioEnd, String ufEnd) {
        this.cdCliente = cdCliente;
        this.nmCliente = nmCliente;
        this.cpfCliente = cpfCliente;
        this.logradouroEnd = logradouroEnd;
        this.numeroEnd = numeroEnd;
        this.bairroEnd = bairroEnd;
        this.municipioEnd = municipioEnd;
        this.ufEnd = ufEnd;
    }

    public ClienteEndereco() {
    }

    public int getCdCliente() {
        return cdCliente;
    }

    public void setCdCliente(int cdCliente) {
        this.cdCliente = cdCliente;
    }

    public String getNmCliente() {
        return nmCliente;
    }

    public void setNmCliente(String nmCliente) {
        this.nmCliente = nmCliente;
    }

    public String getCpfCliente() {
        return cpfCliente;
    }

    public void setCpfCliente(String cpfCliente) {
        this.cpfCliente = cpfCliente;
    }

    public String getLogradouroEnd() {
        return logradouroEnd;
    }

    public void setLogradouroEnd(String logradouroEnd) {
        this.logradouroEnd = logradouroEnd;
    }

    public String getNumeroEnd() {
        return numeroEnd;
    }

    public void setNumeroEnd(String numeroEnd) {
        this.numeroEnd = numeroEnd;
    }

    public String getBairroEnd() {
        return bairroEnd;
    }

    public void setBairroEnd(String bairroEnd) {
        this.bairroEnd = bairroEnd;
    }

    public String getMunicipioEnd() {
        return municipioEnd;
    }

    public void setMunicipioEnd(String municipioEnd) {
        this.municipioEnd = municipioEnd;
    }

    public String getUfEnd() {
        return ufEnd;
    }

    public void setUfEnd(String ufEnd) {
        this.ufEnd = ufEnd;
    }

}
