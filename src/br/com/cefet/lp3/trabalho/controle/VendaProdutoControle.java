package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.VendaProduto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.VendaProdutoDao;
import java.util.List;

public class VendaProdutoControle {

    public String validaDados(VendaProduto vp) throws ControleException, DaoException {
        String retMsg;
        if (vp != null) {
            retMsg = "ok";
        } else {
            retMsg = "VendaVendaProduto nulo";
        }
        return retMsg;
    }

    public int inserir(VendaProduto vp) throws ControleException, DaoException {
        int ret = 1;
        String valida = validaDados(vp);
        if (valida.equalsIgnoreCase("ok")) {
            VendaProdutoDao pDao = new VendaProdutoDao();
            ret = pDao.inserir(vp);
        } else {
            throw new ControleException("Erro ao inserir VendaProduto: " + valida);
        }
        return ret;
    }

    public List<VendaProduto> consultarTodosVendaProduto() throws DaoException {
        List<VendaProduto> ret;
        VendaProdutoDao pDao = new VendaProdutoDao();
        ret = pDao.consultarTodos();
        return ret;
    }

    public List<VendaProduto> consultarVendaProdutoPorCodVenda(int consulta) throws DaoException {
        List<VendaProduto> ret;
        VendaProdutoDao pDao = new VendaProdutoDao();
        ret = pDao.consultarPorCodVenda(consulta);
        return ret;
    }

    public List<VendaProduto> consultarVendaProdutoPorCodProduto(int consulta) throws DaoException {
        List<VendaProduto> ret;
        VendaProdutoDao pDao = new VendaProdutoDao();
        ret = pDao.consultarPorCodProduto(consulta);
        return ret;
    }

    public VendaProduto consultarVendaProdutoPorCod(int cdVenda, int cdProd) throws DaoException {
        VendaProduto ret;
        VendaProdutoDao pDao = new VendaProdutoDao();
        ret = pDao.consultarPorCod(cdVenda, cdProd);
        return ret;
    }

    public int alterar(VendaProduto vp) throws DaoException, ControleException {
        String valida = validaDados(vp);
        if (valida.equalsIgnoreCase("ok")) {
            VendaProdutoDao pDao = new VendaProdutoDao();
            pDao.alterar(vp);
            return 1;
        } else {
            throw new ControleException("Erro ao inserir VendaProduto: " + valida);
        }
    }

    public boolean excluir(VendaProduto vp) throws DaoException, ControleException {
        VendaProdutoDao pDao = new VendaProdutoDao();
        boolean ret = pDao.excluir(vp);
        return ret;
    }
}
