package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.entidade.ClienteEndereco;
import br.com.cefet.lp3.trabalho.entidade.Endereco;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.entidade.Pagamento;
import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.entidade.ProdutoVenda;
import br.com.cefet.lp3.trabalho.entidade.Venda;
import br.com.cefet.lp3.trabalho.entidade.VendaProduto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import java.util.List;

public class Supermercado {

    private String nome = "Merca Djin";
    private final ClienteControle ctlCliente;
    private final EnderecoControle ctlEndereco;
    private final FuncionarioControle ctlFuncionario;
    private final PagamentoControle ctlPagamento;
    private final ProdutoControle ctlProduto;
    private final VendaControle ctlVenda;
    private final VendaProdutoControle ctlVendaProduto;
    private final ClienteEnderecoControle ctlClienteEndereco;
    private final ProdutoVendaControle ctlProdutoVenda;
    private Funcionario funcionarioLogado;
    private static Supermercado instance;

    private Supermercado() {
        ctlCliente = new ClienteControle();
        ctlEndereco = new EnderecoControle();
        ctlFuncionario = new FuncionarioControle();
        ctlPagamento = new PagamentoControle();
        ctlProduto = new ProdutoControle();
        ctlVenda = new VendaControle();
        ctlVendaProduto = new VendaProdutoControle();
        ctlClienteEndereco = new ClienteEnderecoControle();
        ctlProdutoVenda = new ProdutoVendaControle();
        funcionarioLogado = new Funcionario();
    }

    public static Supermercado getInstance() {
        if (instance == null) {
            instance = new Supermercado();
        }
        return instance;

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if (nome != null) {
            this.nome = nome;
        }
    }

    public int inserir(Cliente c) throws ControleException, DaoException {
        return ctlCliente.inserir(c);
    }

    public Cliente consultarClientePorCod(int consulta) throws DaoException {
        return ctlCliente.consultarClientePorCod(consulta);
    }

    public List<Cliente> consultarClientePorNome(String consulta) throws DaoException {
        return ctlCliente.consultarClientePorNome(consulta);
    }

    public List<Cliente> consultarTodosClientes() throws DaoException {
        return ctlCliente.consultarTodosClientes();
    }

    public int alterar(Cliente c) throws DaoException, ControleException {
        return ctlCliente.alterar(c);
    }

    public boolean excluir(Cliente c) throws DaoException, ControleException {
        return ctlCliente.excluir(c);
    }

    public int inserir(Endereco e) throws ControleException, DaoException {
        return ctlEndereco.inserir(e);
    }

    public List<Endereco> consultarEnderecoPorCodCliente(int consulta) throws DaoException {
        return ctlEndereco.consultarEnderecoPorCodCliente(consulta);
    }

    public Endereco consultarEnderecoPorCod(int cd) throws DaoException {
        return ctlEndereco.consultarEnderecoPorCod(cd);
    }

    public int alterar(Endereco e) throws DaoException, ControleException {
        return ctlEndereco.alterar(e);
    }

    public boolean excluir(Endereco e) throws DaoException, ControleException {
        return ctlEndereco.excluir(e);
    }

    public int inserir(Funcionario f) throws ControleException, DaoException {
        return ctlFuncionario.inserir(f);
    }

    public Funcionario consultarFuncionarioPorCod(int consulta) throws DaoException {
        return ctlFuncionario.consultarFuncionarioPorCod(consulta);
    }

    public List<Funcionario> consultarFuncionarioPorNome(String consulta) throws DaoException {
        return ctlFuncionario.consultarFuncionarioPorNome(consulta);
    }

    public List<Funcionario> consultarTodosFuncionario() throws DaoException {
        return ctlFuncionario.consultarTodosFuncionario();
    }

    public int alterar(Funcionario f) throws DaoException, ControleException {
        return ctlFuncionario.alterar(f);
    }

    public boolean excluir(Funcionario f) throws DaoException, ControleException {
        return ctlFuncionario.excluir(f);
    }

    public int inserir(Pagamento pg) throws ControleException, DaoException {
        return ctlPagamento.inserir(pg);
    }

    public Pagamento consultarPagamentoPorCod(int consulta) throws DaoException {
        return ctlPagamento.consultarPagamentoPorCod(consulta);
    }

    public List<Pagamento> consultarPagamentoPorCodVenda(int consulta) throws DaoException {
        return ctlPagamento.consultarPagamentoPorCodVenda(consulta);
    }

    public List<Pagamento> consultarTodosPagamento() throws DaoException {
        return ctlPagamento.consultarTodosPagamento();
    }

    public int alterar(Pagamento pg) throws DaoException, ControleException {
        return ctlPagamento.alterar(pg);
    }

    public boolean excluir(Pagamento pg) throws DaoException, ControleException {
        return ctlPagamento.excluir(pg);
    }

    public int inserir(Produto p) throws ControleException, DaoException {

        return ctlProduto.inserir(p);
    }

    public Produto consultarProdutoPorCod(int consulta) throws DaoException {
        return ctlProduto.consultarProdutoPorCod(consulta);
    }

    public List<Produto> consultarProdutoPorNome(String consulta) throws DaoException {
        return ctlProduto.consultarProdutoPorNome(consulta);
    }

    public List<Produto> consultarProdutoPorCategoria(String consulta) throws DaoException {
        return ctlProduto.consultarProdutoPorCategoria(consulta);
    }

    public List<Produto> consultarTodosProduto() throws DaoException {
        return ctlProduto.consultarTodosProduto();
    }

    public int alterar(Produto p) throws DaoException, ControleException {
        return ctlProduto.alterar(p);
    }

    public boolean excluir(Produto p) throws DaoException, ControleException {
        return ctlProduto.excluir(p);
    }

    public int inserir(Venda v) throws ControleException, DaoException {
        return ctlVenda.inserir(v);
    }

    public Venda consultarVendaPorCod(int consulta) throws DaoException {
        return ctlVenda.consultarVendaPorCod(consulta);
    }

    public List<Venda> consultarVendaPorCodCliente(int consulta) throws DaoException {
        return ctlVenda.consultarVendaPorCodCliente(consulta);
    }

    public List<Venda> consultarTodosVenda() throws DaoException {
        return ctlVenda.consultarTodosVenda();
    }

    public int alterar(Venda v) throws DaoException, ControleException {
        return ctlVenda.alterar(v);
    }

    public boolean excluir(Venda v) throws DaoException, ControleException {
        return ctlVenda.excluir(v);
    }

    public int inserir(VendaProduto vp) throws ControleException, DaoException {
        return ctlVendaProduto.inserir(vp);
    }

    public List<VendaProduto> consultarVendaProdutoPorCodVenda(int consulta) throws DaoException {
        return ctlVendaProduto.consultarVendaProdutoPorCodVenda(consulta);
    }

    public List<VendaProduto> consultarVendaProdutoPorCodProduto(int consulta) throws DaoException {
        return ctlVendaProduto.consultarVendaProdutoPorCodProduto(consulta);
    }

    public VendaProduto consultarVendaProdutoPorCod(int cdVenda, int cdProd) throws DaoException {
        return ctlVendaProduto.consultarVendaProdutoPorCod(cdVenda, cdProd);
    }

    public List<VendaProduto> consultarTodosVendaProduto() throws DaoException {
        return ctlVendaProduto.consultarTodosVendaProduto();
    }

    public int alterar(VendaProduto vp) throws DaoException, ControleException {
        return ctlVendaProduto.alterar(vp);
    }

    public boolean excluir(VendaProduto vp) throws DaoException, ControleException {
        return ctlVendaProduto.excluir(vp);
    }

    public Funcionario getFuncionarioLogado() {
        return funcionarioLogado;
    }

    public void setFuncionarioLogado(Funcionario funcionarioLogado) {
        this.funcionarioLogado = funcionarioLogado;
    }

    public int getProximoCodigoFuncionario() throws DaoException {
        return ctlFuncionario.proximoCodigo();
    }

    public int getProximoCodigoCliente() throws DaoException {
        return ctlCliente.proximoCodigo();
    }

    public int getProximoCodigoEndereco() throws DaoException {
        return ctlEndereco.proximoCodigo();
    }

    public int getProximoCodigoProduto() throws DaoException {
        return ctlProduto.proximoCodigo();
    }

    public int getProximoCodigoVenda() throws DaoException {
        return ctlVenda.proximoCodigo();
    }

    public List<Produto> filtroProduto(String text) throws DaoException {
        return ctlProduto.filtro(text);
    }

    public List<Cliente> filtroCliente(String text) throws DaoException {
        return ctlCliente.filtro(text);
    }

//    public List<Venda> filtroVenda(String text) throws DaoException {
//        return ctlVenda.filtro(text);
//    }
    public List<Funcionario> filtroFuncionario(String text) throws DaoException {
        return ctlFuncionario.filtro(text);
    }

    public List<Produto> relatorioProduto(Double precoMin, Double precoMax, String categoria) throws DaoException {
        return ctlProduto.relatorio(precoMin, precoMax, categoria);
    }

    public List<Venda> relatorioVenda(Double precoMin, Double precoMax, String status) throws DaoException {
        return ctlVenda.relatorio(precoMin, precoMax, status);
    }

    public List<ClienteEndereco> relatorioClienteEndereco(String bairro, String cidade, String cliente) throws DaoException {
        return ctlClienteEndereco.relatorioClienteEndereco(bairro, cidade, cliente);
    }

    public List<ProdutoVenda> relatorioProdutoVenda(int qtdMin, int qtdMax, String produto) throws DaoException {
        return ctlProdutoVenda.relatorioProdutoVenda(qtdMin, qtdMax, produto);
    }
}
