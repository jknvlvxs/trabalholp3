/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.ClienteEndereco;
import br.com.cefet.lp3.trabalho.repositorio.ClienteEnderecoDao;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import java.util.List;

/**
 *
 * @author Júlio
 */
class ClienteEnderecoControle {

    List<ClienteEndereco> relatorioClienteEndereco(String bairro, String cidade, String cliente) throws DaoException {
        ClienteEnderecoDao cDao = new ClienteEnderecoDao();
        return cDao.relatorio(bairro, cidade, cliente);
    }

}
