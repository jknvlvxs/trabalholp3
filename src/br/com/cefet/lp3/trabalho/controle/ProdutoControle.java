package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.Produto;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.ProdutoDao;
import java.util.List;

public class ProdutoControle {

    public String validaDados(Produto p) throws ControleException, DaoException {
        String retMsg;
        if (p != null) {
            retMsg = "ok";
        } else {
            retMsg = "Produto nulo";
        }
        return retMsg;
    }

    public int inserir(Produto p) throws ControleException, DaoException {
        int ret = 1;
        String valida = validaDados(p);
        if (valida.equalsIgnoreCase("ok")) {
            ProdutoDao pDao = new ProdutoDao();
            ret = pDao.inserir(p);
        } else {
            throw new ControleException("Erro ao inserir Produto: " + valida);
        }
        return ret;
    }

    public Produto consultarProdutoPorCod(int cod) throws DaoException {
        Produto ret;
        ProdutoDao pDao = new ProdutoDao();
        ret = pDao.consultarPorCod(cod);
        return ret;
    }

    public List<Produto> consultarProdutoPorNome(String nome) throws DaoException {
        List<Produto> ret;
        ProdutoDao pDao = new ProdutoDao();
        ret = pDao.consultarPorNome(nome);
        return ret;
    }

    public List<Produto> consultarProdutoPorCategoria(String cat) throws DaoException {
        List<Produto> ret;
        ProdutoDao pDao = new ProdutoDao();
        ret = pDao.consultarPorCategoria(cat);
        return ret;
    }

    public List<Produto> consultarTodosProduto() throws DaoException {
        List<Produto> ret;
        ProdutoDao pDao = new ProdutoDao();
        ret = pDao.consultarTodos();
        return ret;
    }

    public int alterar(Produto p) throws DaoException, ControleException {
        String valida = validaDados(p);
        if (valida.equalsIgnoreCase("ok")) {
            ProdutoDao pDao = new ProdutoDao();
            pDao.alterar(p);
            return 1;
        } else {
            throw new ControleException("Erro ao inserir Produto: " + valida);
        }
    }

    public boolean excluir(Produto p) throws DaoException, ControleException {
        ProdutoDao pDao = new ProdutoDao();
        return pDao.excluir(p);
    }

    int proximoCodigo() throws DaoException {
        ProdutoDao pDao = new ProdutoDao();
        return pDao.proximoCodigo();
    }

    List<Produto> filtro(String text) throws DaoException {
        ProdutoDao pDao = new ProdutoDao();
        return pDao.filtro(text);
    }

    List<Produto> relatorio(Double precoMin, Double precoMax, String categoria) throws DaoException {
        ProdutoDao pDao = new ProdutoDao();
        return pDao.relatorio(precoMin, precoMax, categoria);
    }
}
