package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.repositorio.ClienteDao;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.util.Validacao;
import java.util.List;

public class ClienteControle {

    private String validaDados(Cliente c, String origem) throws ControleException, DaoException {
        String retMsg;
        if (c != null) {
            if (c.getNmCliente().length() > 2) {
                if (Validacao.validaCPF(c.getCpfCliente())) {
                    if (Validacao.CPFExistente(c.getCpfCliente(), origem)) {
                        if (c.getTelCliente().length() >= 8) {
                            if (Validacao.UsuarioClienteExistente(c.getUsuarioCliente(), origem)) {
                                retMsg = "ok";
                            } else {
                                retMsg = "Usuário já cadastrado";
                            }
                        } else {
                            retMsg = "Telefone inválido";
                        }
                    } else {
                        retMsg = "CPF já cadastrado";
                    }
                } else {
                    retMsg = "CPF inválido";
                }
            } else {
                retMsg = "Nome inválido";
            }
        } else {
            retMsg = "Cliente nulo";

        }
        return retMsg;
    }

    public int inserir(Cliente c) throws ControleException, DaoException {
        int ret = 1;
        String valida = validaDados(c, "inserir");
        if (valida.equalsIgnoreCase("ok")) {
            ClienteDao cDao = new ClienteDao();
            ret = cDao.inserir(c);
        } else {
            throw new ControleException("Erro ao inserir cliente: " + valida);
        }
        return ret;
    }

    public Cliente consultarClientePorCod(int cod) throws DaoException {
        Cliente ret;
        ClienteDao cDao = new ClienteDao();
        ret = cDao.consultarClientePorCod(cod);
        return ret;
    }

    public List<Cliente> consultarClientePorNome(String nome) throws DaoException {
        List<Cliente> ret;
        ClienteDao cDao = new ClienteDao();
        ret = cDao.consultarClientePorNome(nome);
        return ret;
    }

    public List<Cliente> consultarTodosClientes() throws DaoException {
        List<Cliente> ret;
        ClienteDao cDao = new ClienteDao();
        ret = cDao.consultarTodosClientes();
        return ret;
    }

    public int alterar(Cliente c) throws DaoException, ControleException {
        String valida = validaDados(c, "alterar");
        if (valida.equalsIgnoreCase("ok")) {
            ClienteDao cDao = new ClienteDao();
            cDao.alterar(c);
            return 1;
        } else {
            throw new ControleException("Erro ao inserir cliente: " + valida);
        }
    }

    public boolean excluir(Cliente c) throws DaoException, ControleException {
        ClienteDao cDao = new ClienteDao();
        boolean ret = cDao.excluir(c);
        return ret;
    }

    public int proximoCodigo() throws DaoException {
        ClienteDao cDao = new ClienteDao();
        return cDao.proximoCodigo();
    }

    List<Cliente> filtro(String text) throws DaoException {
        ClienteDao cDao = new ClienteDao();
        return cDao.filtro(text);
    }
}
