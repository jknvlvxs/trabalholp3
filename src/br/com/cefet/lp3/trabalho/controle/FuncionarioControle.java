package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.FuncionarioDao;
import br.com.cefet.lp3.trabalho.util.Validacao;
import java.util.List;

public class FuncionarioControle {

    public String validaDados(Funcionario f, String origem) throws ControleException, DaoException {
        String retMsg;
        if (f != null) {
            if (Validacao.UsuarioFuncionarioExistente(f.getUsuarioFuncionario(), origem)) {
                if (f.getNmFuncionario().length() > 1 && f.getNmFuncionario().length() <= 45) {
                    retMsg = "ok";
                } else {
                    retMsg = "Nome inválido";
                }
            } else {
                retMsg = "Usuario já existente";
            }
        } else {
            retMsg = "Funcionario nulo";
        }
        return retMsg;
    }

    public int inserir(Funcionario f) throws ControleException, DaoException {
        int ret = 1;
        String valida = validaDados(f, "inserir");
        if (valida.equalsIgnoreCase("ok")) {
            FuncionarioDao fDao = new FuncionarioDao();
            ret = fDao.inserir(f);
        } else {
            throw new ControleException("Erro ao inserir Funcionario: " + valida);
        }
        return ret;
    }

    public Funcionario consultarFuncionarioPorCod(int cod) throws DaoException {
        Funcionario ret;
        FuncionarioDao fDao = new FuncionarioDao();
        ret = fDao.consultarFuncionarioPorCod(cod);
        if (ret.getUsuarioFuncionario().equals("admin")) {
            return null;
        }
        return ret;
    }

    public List<Funcionario> consultarFuncionarioPorNome(String nome) throws DaoException {
        List<Funcionario> ret;
        FuncionarioDao fDao = new FuncionarioDao();
        ret = fDao.consultarFuncionarioPorNome(nome);
        for (int i = 0; i < ret.size(); i++) {
            if (ret.get(i).getUsuarioFuncionario().equals("admin")) {
                ret.remove(i);
            }
        }
        return ret;
    }

    public List<Funcionario> consultarTodosFuncionario() throws DaoException {
        List<Funcionario> ret;
        FuncionarioDao fDao = new FuncionarioDao();
        ret = fDao.consultarTodosFuncionario();
//        for (int i = 0; i < ret.size(); i++) {
//            if (ret.get(i).getUsuarioFuncionario().equals("admin")) {
//                ret.remove(i);
//            }
//        }
        return ret;
    }

    public int proximoCodigo() throws DaoException {
        FuncionarioDao fDao = new FuncionarioDao();
        int ret = fDao.proximoCodigo();
        return ret;
    }

    public int alterar(Funcionario f) throws DaoException, ControleException {
        String valida = validaDados(f, "alterar");
        if (valida.equalsIgnoreCase("ok")) {
            FuncionarioDao fDao = new FuncionarioDao();
            fDao.alterar(f);
            return 1;
        } else {
            throw new ControleException("Erro ao inserir Funcionario: " + valida);
        }
    }

    public boolean excluir(Funcionario f) throws DaoException, ControleException {
        FuncionarioDao fDao = new FuncionarioDao();
        return fDao.excluir(f);
    }

    List<Funcionario> filtro(String text) throws DaoException {
        FuncionarioDao fDao = new FuncionarioDao();
        return fDao.filtro(text);
    }
}
