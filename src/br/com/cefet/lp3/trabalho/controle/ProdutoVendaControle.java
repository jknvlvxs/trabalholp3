/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.ProdutoVenda;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.ProdutoVendaDao;
import java.util.List;

/**
 *
 * @author Júlio
 */
class ProdutoVendaControle {

    List<ProdutoVenda> relatorioProdutoVenda(int qtdMin, int qtdMax, String produto) throws DaoException {
        ProdutoVendaDao pDao = new ProdutoVendaDao();
        return pDao.relatorio(qtdMin, qtdMax, produto);
    }

}
