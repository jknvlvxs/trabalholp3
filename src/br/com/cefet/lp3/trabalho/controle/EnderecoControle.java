package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.Endereco;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.EnderecoDao;
import java.util.List;

public class EnderecoControle {

    public String validaDados(Endereco e) throws ControleException, DaoException {
        String retMsg;
        if (e != null) {
            if (e.getLogradouroEnd() != null && e.getLogradouroEnd().length() > 2) {
                if (e.getBairroEnd() != null && e.getBairroEnd().length() > 5) {
                    if (e.getUfEnd().length() == 2) {
                        if (e.getCepEnd().length() <= 9) {
                            retMsg = "ok";
                        } else {
                            retMsg = "CEP inválido";
                        }
                    } else {
                        retMsg = "UF inválida";
                    }
                } else {
                    retMsg = "Bairro inválido";
                }
            } else {
                retMsg = "Logradouro inválido";
            }
        } else {
            retMsg = "Endereco nulo";
        }
        return retMsg;
    }

    public int inserir(Endereco e) throws ControleException, DaoException {
        int ret = 1;
        String valida = validaDados(e);
        if (valida.equalsIgnoreCase("ok")) {
            EnderecoDao eDao = new EnderecoDao();
            ret = eDao.inserir(e);
        } else {
            throw new ControleException("Erro ao inserir endereco: " + valida);
        }
        return ret;
    }

    public List<Endereco> consultarEnderecoPorCodCliente(int cod) throws DaoException {
        List<Endereco> ret;
        EnderecoDao eDao = new EnderecoDao();
        ret = eDao.consultarEnderecoPorCodCliente(cod);
        return ret;
    }

    public Endereco consultarEnderecoPorCod(int cod) throws DaoException {
        Endereco ret;
        EnderecoDao eDao = new EnderecoDao();
        ret = eDao.consultarEnderecoPorCod(cod);
        return ret;
    }

    public List<Endereco> consultarTodosEndereco() throws DaoException {
        List<Endereco> ret;
        EnderecoDao eDao = new EnderecoDao();
        ret = eDao.consultarTodos();
        return ret;
    }

    public int alterar(Endereco e) throws DaoException, ControleException {
        String valida = validaDados(e);
        if (valida.equalsIgnoreCase("ok")) {
            EnderecoDao eDao = new EnderecoDao();
            eDao.alterar(e);
            return 1;
        } else {
            throw new ControleException("Erro ao alterar endereco: " + valida);
        }
    }

    public boolean excluir(Endereco e) throws DaoException, ControleException {
        EnderecoDao eDao = new EnderecoDao();
        boolean ret = eDao.excluir(e);
        return ret;
    }

    int proximoCodigo() throws DaoException {
        EnderecoDao eDao = new EnderecoDao();
        return eDao.proximoCodigo();
    }
}
