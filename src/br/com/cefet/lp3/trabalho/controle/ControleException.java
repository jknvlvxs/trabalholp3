package br.com.cefet.lp3.trabalho.controle;

public class ControleException extends Exception {

    public ControleException(String message) {
        super(message);
    }

    public ControleException(String message, Throwable cause) {
        super(message, cause);
    }

}
