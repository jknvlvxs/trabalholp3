package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.Venda;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.VendaDao;
import java.util.List;

public class VendaControle {

    public String validaDados(Venda v) throws ControleException, DaoException {
        String retMsg;
        if (v != null) {
            retMsg = "ok";
        } else {
            retMsg = "Venda nulo";
        }
        return retMsg;
    }

    public int inserir(Venda v) throws ControleException, DaoException {
        int ret = 1;
        String valida = validaDados(v);
        if (valida.equalsIgnoreCase("ok")) {
            VendaDao vDao = new VendaDao();
            ret = vDao.inserir(v);
        } else {
            throw new ControleException("Erro ao inserir Venda: " + valida);
        }
        return ret;
    }

    public Venda consultarVendaPorCod(int cod) throws DaoException {
        Venda ret;
        VendaDao vDao = new VendaDao();
        ret = vDao.consultarVendaPorCod(cod);
        return ret;
    }

    public List<Venda> consultarVendaPorCodCliente(int consulta) throws DaoException {
        List<Venda> ret;
        VendaDao vDao = new VendaDao();
        ret = vDao.consultarVendaPorCodCliente(consulta);
        return ret;
    }

    public List<Venda> consultarTodosVenda() throws DaoException {
        List<Venda> ret;
        VendaDao vDao = new VendaDao();
        ret = vDao.consultarTodosVenda();
        return ret;
    }

    public int alterar(Venda v) throws DaoException, ControleException {
        String valida = validaDados(v);
        if (valida.equalsIgnoreCase("ok")) {
            VendaDao vDao = new VendaDao();
            vDao.alterar(v);
            return 1;
        } else {
            throw new ControleException("Erro ao inserir Venda: " + valida);
        }
    }

    public boolean excluir(Venda v) throws DaoException, ControleException {
        VendaDao vDao = new VendaDao();
        return vDao.excluir(v);
    }

    public int proximoCodigo() throws DaoException {
        VendaDao vDao = new VendaDao();
        return vDao.proximoCodigo();
    }

    public List<Venda> relatorio(Double precoMin, Double precoMax, String status) throws DaoException {
        VendaDao vDao = new VendaDao();
        return vDao.relatorio(precoMin, precoMax, status);
    }
}
