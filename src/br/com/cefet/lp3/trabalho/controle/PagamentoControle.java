package br.com.cefet.lp3.trabalho.controle;

import br.com.cefet.lp3.trabalho.entidade.Pagamento;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.PagamentoDao;
import java.util.List;

public class PagamentoControle {

    public String validaDados(Pagamento p) throws ControleException, DaoException {
        String retMsg;
        if (p != null) {
            retMsg = "ok";
        } else {
            retMsg = "Pagamento nulo";
        }
        return retMsg;
    }

    public int inserir(Pagamento p) throws ControleException, DaoException {
        int ret = 1;
        String valida = validaDados(p);
        if (valida.equalsIgnoreCase("ok")) {
            PagamentoDao pDao = new PagamentoDao();
            ret = pDao.inserir(p);
        } else {
            throw new ControleException("Erro ao inserir Pagamento: " + valida);
        }
        return ret;
    }

    public Pagamento consultarPagamentoPorCod(int cod) throws DaoException {
        Pagamento ret;
        PagamentoDao pDao = new PagamentoDao();
        ret = pDao.consultarPagamentoPorCod(cod);
        return ret;
    }

    public List<Pagamento> consultarPagamentoPorCodVenda(int cod) throws DaoException {
        List<Pagamento> ret;
        PagamentoDao pDao = new PagamentoDao();
        ret = pDao.consultarPagamentoPorCodVenda(cod);
        return ret;
    }

    public List<Pagamento> consultarTodosPagamento() throws DaoException {
        List<Pagamento> ret;
        PagamentoDao pDao = new PagamentoDao();
        ret = pDao.consultarTodosPagamento();
        return ret;
    }

    public int alterar(Pagamento p) throws DaoException, ControleException {
        String valida = validaDados(p);
        if (valida.equalsIgnoreCase("ok")) {
            PagamentoDao pDao = new PagamentoDao();
            pDao.alterar(p);
            return 1;
        } else {
            throw new ControleException("Erro ao inserir Pagamento: " + valida);
        }
    }

    public boolean excluir(Pagamento p) throws DaoException, ControleException {
        PagamentoDao pDao = new PagamentoDao();
        boolean ret = pDao.excluir(p);
        return ret;
    }
}
