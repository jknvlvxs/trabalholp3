package br.com.cefet.lp3.trabalho.util;

import br.com.cefet.lp3.trabalho.controle.Supermercado;
import br.com.cefet.lp3.trabalho.entidade.Cliente;
import br.com.cefet.lp3.trabalho.entidade.Funcionario;
import br.com.cefet.lp3.trabalho.repositorio.DaoException;
import br.com.cefet.lp3.trabalho.repositorio.FuncionarioDao;
import java.util.List;

public class Validacao {

    public static boolean validaCPF(String cpf) {
        cpf = cpf.replace(".", "");
        cpf = cpf.replace("-", "");
        if (cpf.length() != 11) {
            return false;
        }

        boolean isIgual;
        boolean numRep = true;
        for (int i = 0; i < 11; i++) {
            String valor = String.valueOf(cpf.charAt(0));
            if (valor.equals(String.valueOf(cpf.charAt(i)))) {
            } else {
                numRep = false;
            }
        }

        isIgual = numRep;
        if (isIgual == true) {
            return false;
        } else {
            // Primeiro Digito Verificador
            int soma = 0;

            int mult = 10;
            for (int i = 0; i < 9; i++) {
                String valor = String.valueOf(cpf.charAt(i));
                int digito = Integer.parseInt(valor);
                soma = soma + (digito * mult);
                mult--;
            }

            int resto = soma % 11;
            int digitoUm = 0;
            if (resto >= 2) {
                digitoUm = 11 - resto;
            }

            // Segundo Digito Verificador
            soma = 0;
            mult = 11;
            for (int j = 0; j < 10; j++) {
                String valor = String.valueOf(cpf.charAt(j));
                int digito = Integer.parseInt(valor);
                soma = soma + (digito * mult);
                mult--;
            }
            resto = soma % 11;
            int digitoDois = 0;

            if (resto >= 2) {
                digitoDois = 11 - resto;
            }

            return digitoUm == Integer.parseInt(cpf.charAt(9) + "")
                    && digitoDois == Integer.parseInt(cpf.charAt(10) + "");
        }
    }

    public static boolean CPFExistente(String cpf, String origem) throws DaoException {
        cpf = cpf.replace(".", "");
        cpf = cpf.replace("-", "");
        Supermercado s = Supermercado.getInstance();
        List<Cliente> clientes = s.consultarTodosClientes();
        if (origem.equals("inserir")) {
            for (int i = 0; i < clientes.size(); i++) {
                if (cpf.equals(clientes.get(i).getCpfCliente())) {
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean FuncionarioVazio() throws DaoException {
        FuncionarioDao fD = new FuncionarioDao();
        List<Funcionario> fList = fD.consultarTodosFuncionario();
        return fList.isEmpty();
    }

    public static boolean UsuarioClienteExistente(String usuarioCliente, String origem) throws DaoException {
        Supermercado s = Supermercado.getInstance();
        List<Cliente> clientes = s.consultarTodosClientes();
        if (origem.equals("inserir")) {
            for (int i = 0; i < clientes.size(); i++) {
                if (usuarioCliente.equals(clientes.get(i).getUsuarioCliente())) {
                    return false;
                }
            }
        }

        return true;
    }

    public static boolean UsuarioFuncionarioExistente(String usuarioCliente, String origem) throws DaoException {
        Supermercado s = Supermercado.getInstance();
        List<Funcionario> func = s.consultarTodosFuncionario();
        boolean retorno = true;
        if (origem.equals("inserir")) {
            for (int i = 0; i < func.size(); i++) {
                if (usuarioCliente.equals(func.get(i).getUsuarioFuncionario())) {
                    return false;
                }
            }
        }

        return retorno;
    }
}
